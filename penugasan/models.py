from django.db import models
from django.db import connection

# Create your models here.
SCHEMA = "bike_sharing"

PENUGASAN = SCHEMA + "." + "penugasan"
PERSON = SCHEMA + "." + "person"
PETUGAS = SCHEMA + "." + "petugas"
STASIUN = SCHEMA + "." + "stasiun"

cursor =  connection.cursor()

class Penugasan:

    def get_penugasan():
        query = 'select * from %s ;' % (PENUGASAN)
        cursor.execute(query)
        return cursor

    def get_nama_petugas():
        query = '''select pt.ktp, pr.nama
                from %s as pt, %s as pr
                where pt.ktp=pr.ktp;
                ''' % (PETUGAS, PERSON)
        cursor.execute(query)
        return cursor

    def get_nama_stasiun():
        query = '''select nama
                from %s;
                ''' % (STASIUN)
        cursor.execute(query)
        return cursor

    def get_penugasan_person_stasiun():
        query = '''select pr.nama, png.ktp, cast(png.start_datetime as date), 
                cast(png.end_datetime as date), s.nama as stasiun, png.id_stasiun
                from %s as pr, %s as png, %s as s, %s as pt
                where png.id_stasiun=s.id_stasiun
                and png.ktp=pt.ktp and pt.ktp=pr.ktp;
                ''' % (PERSON, PENUGASAN, STASIUN, PETUGAS)
        cursor.execute(query)
        return cursor

    def get_penugasan_person_stasiun_by_ktp_and_id_stasiun(no_ktp, stasiun_id):
        query = '''select pr.nama, png.ktp, cast(png.start_datetime as date), 
                cast(png.end_datetime as date), s.nama as stasiun, png.id_stasiun
                from %s as pr, %s as png, %s as s, %s as pt
                where png.id_stasiun=s.id_stasiun
                and png.ktp=pt.ktp and pt.ktp=pr.ktp and png.ktp='%s' and png.id_stasiun='%s';
                ''' % (PERSON, PENUGASAN, STASIUN, PETUGAS, no_ktp, stasiun_id)
        cursor.execute(query)
        return cursor

    def insert_penugasan(penugasan):
        query =  '''
            INSERT INTO %s (ktp, start_datetime, id_stasiun, end_datetime)
            VALUES ('%s', '%s', '%s', '%s');
        ''' %(PENUGASAN, penugasan['ktp'], penugasan['start_datetime'],
              penugasan['id_stasiun'], penugasan['end_datetime'])
        print(query)
        cursor.execute(query)
        return cursor

    def update_penugasan(penugasan, no_ktp, id_stasiun):
        query =  '''
            UPDATE %s
            set ktp = '%s', start_datetime = '%s', id_stasiun = '%s', end_datetime = '%s'
            where ktp = '%s' and id_stasiun = '%s';
        ''' %(PENUGASAN, penugasan['ktp'], penugasan['start_datetime'], penugasan['id_stasiun'],
              penugasan['end_datetime'], no_ktp, id_stasiun)
        cursor.execute(query)
        return cursor

    def delete_penugasan(no_ktp, id_stasiun):
        query =  '''
            DELETE FROM %s WHERE ktp = '%s' and id_stasiun = '%s';
        ''' %(PENUGASAN, no_ktp, id_stasiun)
        cursor.execute(query)
        return cursor

