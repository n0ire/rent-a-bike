from django.shortcuts import render, redirect
from .models import Penugasan
from stasiun.models import Stasiun

def get_as_dict(cursor):
    columns = [column[0] for column in cursor.description]
    results = []
    for row in cursor.fetchall():
        results.append(dict(zip(columns, row)))
    return results


def list(request):
    context = {
        "assignments" : get_as_dict(Penugasan.get_penugasan_person_stasiun()),
        "stations" : get_as_dict(Stasiun.get_all_stasiun())
    }
    return render(request, 'penugasan-list.html', context)

def edit(request, ktp, id_stasiun):
    context = {
        "assignments": get_as_dict(Penugasan.get_penugasan_person_stasiun_by_ktp_and_id_stasiun(ktp, id_stasiun)),
        "stations": get_as_dict(Stasiun.get_all_stasiun()),
        "officers": get_as_dict(Penugasan.get_nama_petugas()),
    }
    print(context["assignments"])
    return render(request, 'penugasan-update.html', context)

def add(request):
    context = {
        "officers" : get_as_dict(Penugasan.get_nama_petugas()),
        "stations" : get_as_dict(Stasiun.get_all_stasiun())
    }
    return render(request, 'penugasan-add.html', context)

def store(request):
    penugasan = {
            "ktp": request.POST.get('petugas'),
            "start_datetime": request.POST.get('tgl_mulai'),
            "end_datetime": request.POST.get('tgl_akhir'),
            "id_stasiun": request.POST.get('stasiun'),
    }
    Penugasan.insert_penugasan(penugasan)
    return redirect('/penugasan/')

def update(request, ktp, id_stasiun):
    penugasan = {
            "ktp": request.POST.get('petugas'),
            "start_datetime": request.POST.get('tgl_mulai'),
            "end_datetime": request.POST.get('tgl_akhir'),
            "id_stasiun": request.POST.get('stasiun'),
    }
    Penugasan.update_penugasan(penugasan, ktp, id_stasiun)
    return redirect('/penugasan/')

def delete(request, ktp, id_stasiun):
    Penugasan.delete_penugasan(ktp, id_stasiun)
    return redirect('/penugasan/')


