from django.contrib import admin
from django.urls import path, include, reverse
from django.conf import settings
from django.conf.urls.static import static

from . import views

urlpatterns = [
    path('', views.list),
    path('edit/<ktp>-<id_stasiun>', views.edit),
    path('add/', views.add),
    path('store/', views.store),
    path('update/<ktp>-<id_stasiun>', views.update),
    path('delete/<ktp>-<id_stasiun>', views.delete)
]

