from django.db import models
from django.db import connection

# Create your models here.
SCHEMA = "bike_sharing"

ACARA = SCHEMA + "." + "acara"
STASIUN = SCHEMA + "." + "stasiun"
ACARA_STASIUN = SCHEMA + "." + "acara_stasiun"

cursor =  connection.cursor()

class Acara:

    def get_acara():
        query = 'select * from %s ;' % (ACARA)
        cursor.execute(query)
        return cursor

    def get_nama_stasiun():
        query = '''select nama
                from %s;
                ''' % (STASIUN)
        cursor.execute(query)
        return cursor

    def get_acara_stasiun():
        query = '''select a.judul, a.deskripsi, a.tgl_mulai, a.tgl_akhir, a.is_free, s.nama as stasiun
                from %s as a, %s as s, %s as sa
                where a.id_acara=sa.id_acara and sa.id_stasiun=s.id_stasiun;
                ''' % (ACARA, STASIUN, ACARA_STASIUN)
        cursor.execute(query)
        return cursor

    def get_acara_stasiun_by_id(acara_id):
        query = '''select a.id_acara, a.judul, a.deskripsi, a.tgl_mulai, a.tgl_akhir, a.is_free, s.nama as stasiun
                from %s as a, %s as s, %s as sa
                where a.id_acara=sa.id_acara and sa.id_stasiun=s.id_stasiun and a.id_acara='%s';
                ''' % (ACARA, STASIUN, ACARA_STASIUN, acara_id)
        cursor.execute(query)
        return cursor

    def insert_acara(acara):
        query =  '''
            INSERT INTO %s (id_acara, judul, deskripsi, tgl_mulai, tgl_akhir, is_free)
            VALUES ('%s', '%s', '%s', '%s', '%s', '%s');
        ''' %(ACARA, acara['id_acara'], acara['judul'], acara['deskripsi'], acara['tgl_mulai'], acara['tgl_akhir'], acara['is_free'])
        print(query)
        cursor.execute(query)
        return cursor

    def insert_acara_stasiun(acara_stasiun):
        query =  '''
            INSERT INTO %s (id_stasiun, id_acara)
            VALUES ('%s', '%s');
        ''' %(ACARA_STASIUN, acara_stasiun['id_stasiun'], acara_stasiun['id_acara'])
        print(query)
        cursor.execute(query)
        return cursor

    def update_acara(acara, id_acara):
        query =  '''
            UPDATE %s
            set judul = '%s', deskripsi = '%s', tgl_mulai = '%s', tgl_akhir = '%s', is_free = '%s'
            where id_acara = '%s';
        ''' %(ACARA, acara['judul'], acara['deskripsi'], acara['tgl_mulai'], acara['tgl_akhir'], acara['is_free'], id_acara)
        cursor.execute(query)
        return cursor

    def update_acara_stasiun(acara_stasiun, id_acara):
        query = '''
                    UPDATE %s
                    set id_stasiun = '%s'
                    where id_acara = '%s';
                ''' % (
        ACARA_STASIUN, acara_stasiun['id_stasiun'], id_acara)
        cursor.execute(query)
        return cursor

    def delete_acara(id_acara):
        query =  '''
            DELETE FROM %s WHERE id_acara = '%s';
        ''' %(ACARA, id_acara)
        cursor.execute(query)
        return cursor