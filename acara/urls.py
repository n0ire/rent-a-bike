from django.contrib import admin
from django.urls import path, include, reverse
from django.conf import settings
from django.conf.urls.static import static

from . import views

urlpatterns = [
    path('', views.list),
    path('update/<id_acara>', views.update),
    path('add/', views.add),
    path('store/', views.store),
    path('edit/<id_acara>/', views.edit),
    path('delete/<id_acara>', views.delete)
]

