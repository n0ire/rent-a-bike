from django.shortcuts import render, redirect
from .models import Acara
from stasiun.models import Stasiun
from random import *
import string

def get_as_dict(cursor):
    columns = [column[0] for column in cursor.description]
    results = []
    for row in cursor.fetchall():
        results.append(dict(zip(columns, row)))
    return results


def list(request):
    context = {
        "events": get_as_dict(Acara.get_acara())
    }
    print(len(context["events"]))
    print(context["events"])
    return render(request, 'acara-list.html', context)

def edit(request, id_acara):
    context = {
        "events": get_as_dict(Acara.get_acara_stasiun_by_id(id_acara)),
        "stations": get_as_dict(Stasiun.get_all_stasiun())
    }
    print(context)
    return render(request, 'acara-update.html', context)

def add(request):
    context = {
        "stations": get_as_dict(Stasiun.get_all_stasiun())
    }
    return render(request, 'acara-add.html', context)

def store(request):
    min_char = 5
    max_char = 10
    allchar = string.ascii_letters + string.digits
    acara_id = "".join(choice(allchar) for x in range(randint(min_char, max_char)))
    if request.POST.get('is_free') == 'Ya':
        free = True
    else:
        free = False
    acara = {
            "id_acara": acara_id,
            "judul": request.POST.get('judul'),
            "deskripsi": request.POST.get('deskripsi'),
            "tgl_mulai": request.POST.get('tgl_mulai'),
            "tgl_akhir": request.POST.get('tgl_akhir'),
            "is_free": free
    }
    acara_stasiun = {
        "id_stasiun": request.POST.get('stasiun'),
        "id_acara": acara_id
    }
    Acara.insert_acara(acara)
    Acara.insert_acara_stasiun(acara_stasiun)
    return redirect('/acara/')

def update(request, id_acara):
    if request.POST.get('is_free') == 'Ya':
        free = True
    else:
        free = False
    acara = {
            "judul": request.POST.get('judul'),
            "deskripsi": request.POST.get('deskripsi'),
            "tgl_mulai": request.POST.get('tgl_mulai'),
            "tgl_akhir": request.POST.get('tgl_akhir'),
            "is_free": free
    }
    acara_stasiun = {
        "id_stasiun": request.POST.get('stasiun')
    }
    Acara.update_acara(acara, id_acara)
    Acara.update_acara_stasiun(acara_stasiun, id_acara)
    return redirect('/acara/')

def delete(request, id_acara):
    Acara.delete_acara(id_acara)
    return redirect('/acara/')