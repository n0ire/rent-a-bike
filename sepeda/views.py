from django.shortcuts import render, redirect
from stasiun.models import Stasiun
from .models import Sepeda

def get_as_dict(cursor):
    columns = [column[0] for column in cursor.description]
    results = []
    for row in cursor.fetchall():
        results.append(dict(zip(columns, row)))
    return results

def index(request):
    context = {
        "sepedas" : get_as_dict(Sepeda.get_sepeda_and_join_stasiun_penyumbang())
    }
    print(len(context["sepedas"]))
    print(context["sepedas"])
    return render(request, 'sepeda-index.html', context)

def edit(request, nomor):
    context = {
        "sepeda" : get_as_dict(Sepeda.get_sepeda_and_join_stasiun_penyumbang_by_nomor(nomor)),
        "stations" : get_as_dict(Stasiun.get_all_stasiun()),
        "anggotas" : get_as_dict(Sepeda.get_all_anggota_x_person())
    }
    print(context['sepeda'])
    return render(request, 'sepeda-edit.html', context)

def create(request):
    context = {
        "stations" : get_as_dict(Stasiun.get_all_stasiun()),
        "anggotas" : get_as_dict(Sepeda.get_all_anggota_x_person())
    }

    print(context["stations"])
    return render(request, 'sepeda-create.html', context)


def store(request):
    sepeda = {
            "merk" : request.POST.get('merk'),
            "jenis" : request.POST.get('jenis'),
            "status" : request.POST.get('status'),
            "id_stasiun" : request.POST.get('stasiun'),
            "no_kartu_penyumbang" : request.POST.get('penyumbang'),
    }
    Sepeda.insert_sepeda(sepeda)
    return redirect('/sepeda/')

def update(request, nomor):
    sepeda = {
            "merk" : request.POST.get('merk'),
            "jenis" : request.POST.get('jenis'),
            "status" : request.POST.get('status'),
            "id_stasiun" : request.POST.get('stasiun'),
            "no_kartu_penyumbang" : request.POST.get('penyumbang'),
    }

    if(len(request.POST.get('merk')) > 10):
        return redirect('/sepeda/')

    Sepeda.update_sepeda(sepeda, nomor)
    return redirect('/sepeda/')


def delete(request, nomor):
    Sepeda.delete_sepeda(nomor)
    return redirect('/sepeda/')
