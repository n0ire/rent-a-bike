from django.contrib import admin
from django.urls import path, include, reverse
from django.conf import settings
from django.conf.urls.static import static

from . import views

urlpatterns = [
    path('', views.index, name="index"),
    path('edit/<int:id>/', views.edit),
    path('create/', views.create),
    path('store/', views.store),
    path('delete/<int:nomor>', views.delete),
    path('update/<int:nomor>', views.update),
    path('edit/<int:nomor>/', views.edit),
]
