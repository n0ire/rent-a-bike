from django.db import models
from django.db import connection

# Create your models here.
SCHEMA = "bike_sharing"

SEPEDA = SCHEMA + "." + "sepeda"
STASIUN = SCHEMA + "." + "stasiun"
ANGGOTA = SCHEMA + "." + "anggota"
PERSON = SCHEMA + "." + "person"
ANGGOTA = SCHEMA + "." + "anggota"

cursor =  connection.cursor()

class Sepeda:

    def get_all_sepeda():
        query = 'select * from %s ;' % (SEPEDA)
        cursor.execute(query)
        return cursor

    def get_sepeda_by_id(id):
        query = 'select * from %s where nomor = \'%d\' ;' % (STASIUN, id)
        cursor.execute(query)
        return cursor

    def get_sepeda_and_join_stasiun_penyumbang():
        query = '''select CAST (sp.nomor AS INTEGER) as nomor, sp.merk as merk,
                    sp.status as status, sp.jenis as jenis,
                    st.nama as nama_st, p.nama as nama_penyumbang
                    from %s as sp,
                    %s as st,
                    %s as a,
                    %s as p
                    where sp.id_stasiun = st.id_stasiun
                    and sp.no_kartu_penyumbang = a.no_kartu
                    and a.ktp = p.ktp
                    order by nomor asc
                    ;
                ''' % (SEPEDA, STASIUN, ANGGOTA, PERSON)
        cursor.execute(query)
        return cursor

    def get_sepeda_and_join_stasiun_penyumbang_by_nomor(nomor):
        query = '''select CAST (sp.nomor AS INTEGER) as nomor, sp.merk as merk,
                    sp.status as status, sp.jenis as jenis, sp.id_stasiun as id_stasiun,
                    st.nama as nama_st, p.nama as nama_penyumbang, sp.no_kartu_penyumbang
                    from %s as sp,
                    %s as st,
                    %s as a,
                    %s as p
                    where sp.id_stasiun = st.id_stasiun
                    and sp.no_kartu_penyumbang = a.no_kartu
                    and a.ktp = p.ktp
                    and sp.nomor = '%s'
                    order by nomor asc
                    ;
                ''' % (SEPEDA, STASIUN, ANGGOTA, PERSON, nomor)
        cursor.execute(query)
        return cursor

    def get_all_anggota_x_person():
        query = 'select nama, no_kartu, ktp from %s natural join %s ;' % (PERSON, ANGGOTA)
        cursor.execute(query)
        return cursor

    def insert_sepeda(sepeda):
        query =  '''
            INSERT INTO %s (merk, jenis, status, id_stasiun, no_kartu_penyumbang)
            VALUES ('%s', '%s', '%s', '%s', '%s');
        ''' %(SEPEDA, sepeda['merk'], sepeda['jenis'], sepeda['status'], sepeda['id_stasiun'], sepeda['no_kartu_penyumbang'])
        print(query)
        cursor.execute(query)
        return cursor

    def update_sepeda(sepeda, nomor):
        query =  '''
            UPDATE %s
            set merk = '%s', jenis = '%s', status = '%s', id_stasiun = '%s', no_kartu_penyumbang = '%s'
            where nomor = '%s';
        ''' %(SEPEDA, sepeda['merk'], sepeda['jenis'], sepeda['status'], sepeda['id_stasiun'], sepeda['no_kartu_penyumbang'], nomor)
        cursor.execute(query)
        return cursor

    def delete_sepeda(nomor):
        query =  '''
            DELETE FROM %s WHERE nomor = '%s';
        ''' %(SEPEDA, nomor)
        cursor.execute(query)
        return cursor
