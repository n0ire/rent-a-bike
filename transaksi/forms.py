from django import forms

nominal_field = forms.DecimalField()

class TopupForm(forms.Form):
    nominal = nominal_field
