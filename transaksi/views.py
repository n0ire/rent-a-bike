from datetime import datetime
from django.shortcuts import render, redirect
from transaksi import forms
from transaksi.models import TransaksiManager, Laporan


# Create your views here.
def topup(request):
    if request.method == 'POST':
        form = forms.TopupForm(request.POST)

        if form.is_valid():
            nominal = form.cleaned_data['nominal']

            topup = TransaksiManager.create(
                    jenis='TopUp',
                    date_time=datetime.now(),
                    no_kartu_anggota=request.user.anggota.no_kartu,
                    nominal=nominal)

        return redirect('/')

    else:
        form = forms.TopupForm()

    context = {
        'form': form,
    }

    return render(request, 'topup.html', context)


def riwayat(request):
    transactions = TransaksiManager.filter(no_kartu_anggota=request.user.anggota.no_kartu)

    context = {
        'transactions': transactions
    }

    return render(request, 'riwayat.html', context)


def laporan(request):
    import datetime
    now = datetime.datetime.now()

    reports = [
        Laporan('5432167890', '1234567890', now, '1357924680',
                '2412242134', 'pinjan'),
        Laporan('5432167891', '1234567891', now, '1357924681',
                '2412242135', 'kembali'),
        Laporan('5432167891', '1234567893', now, '1357924681',
                '2412242136', 'hilang'),
    ]

    context = {
        'reports': reports
    }

    return render(request, 'laporan.html', context)
