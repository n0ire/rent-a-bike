import dataclasses
from datetime import datetime
from django.db import connection


# Create your models here.
SCHEMA = 'BIKE_SHARING'


def fetchall(datacls, cursor):
    "Return all rows from a cursor as list of dataclass instances"
    columns = [col[0] for col in cursor.description]
    return [
        datacls(**dict(zip(columns, row)))
        for row in cursor.fetchall()
    ]


def fetchone(datacls, cursor):
    "Return one row from a cursor as a dataclass instance"
    columns = [col[0] for col in cursor.description]
    fetched = cursor.fetchone()

    if fetched:
        return datacls(**dict(zip(columns, fetched)))


def construct_and_query(datacls, table_name, attrs):
    conditions = []
    params = []

    for attr in attrs:
        filtered = filter(lambda f: f.name == attr,
                          dataclasses.fields(datacls))
        filtered = list(filtered)[0]
        field, typ = filtered.name, filtered.type

        if not isinstance(attrs[field], typ):
            # TODO raise error
            pass

        template = f"{field} = %s"

        conditions.append(template)
        params.append(attrs[attr])

    conditions = ' AND '.join(conditions) or ' TRUE'
    query = "SELECT * FROM %s WHERE %s;" % (table_name, conditions)
    return query, params


def construct_single_insert(datacls, table_name, attrs):
    obj = datacls(**attrs)
    attrs = dataclasses.asdict(obj)
    values = ",".join("%s" for i in range(len(attrs)))
    query = "INSERT INTO %s values (%s);" % (table_name, values)

    return query, list(attrs.values())


@dataclasses.dataclass
class Transaksi:
    no_kartu_anggota: str
    date_time: datetime
    jenis: str
    nominal: float


class TransaksiManager:
    TABLE_NAME = 'TRANSAKSI'
    model = Transaksi

    @classmethod
    def table_name(cls):
        return "%s.%s" % (SCHEMA, cls.TABLE_NAME)

    @classmethod
    def get(cls, **attrs):
        query, params = construct_and_query(cls.model, cls.table_name(), attrs)

        with connection.cursor() as c:
            c.execute(query, params)
            results = fetchone(cls.model, c)

        return results

    @classmethod
    def filter(cls, **attrs):
        query, params = construct_and_query(cls.model, cls.table_name(), attrs)

        with connection.cursor() as c:
            c.execute(query, params)
            results = fetchall(cls.model, c)

        return results

    @classmethod
    def create(cls, **attrs):
        query, params = construct_single_insert(cls.model, cls.table_name(), attrs)

        with connection.cursor() as c:
            c.execute(query, params)

        return cls.model(**attrs)


class Laporan:
    def __init__(self, id_laporan, no_kartu_anggota,
                 datetime_pinjam, no_sepeda, id_stasiun, status):
        self.id_laporan = id_laporan
        self.no_kartu_anggota = no_kartu_anggota
        self.datetime_pinjam = datetime_pinjam
        self.no_sepeda = no_sepeda
        self.id_stasiun = id_stasiun
        self.status = status

    @property
    def peminjaman(self):
        from random import choice

        class Peminjaman:
            def __init__(self):
                self.denda = choice((50_000, 100_000, 500_000))
                self.anggota = choice(('Nata', 'Dhita', 'Nio'))

        return Peminjaman()
