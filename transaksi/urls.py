from django.urls import path
from transaksi import views

urlpatterns = [
    path('topup', views.topup, name='topup'),
    path('riwayat', views.riwayat, name='riwayat'),
    path('laporan', views.laporan, name='laporan'),
]
