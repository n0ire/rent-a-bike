from dataclasses import asdict
from django.core.serializers.json import DjangoJSONEncoder
from gauth.models import PersonManager
import json


def authenticate(request, **credentials):
    user = PersonManager.get(**credentials)

    if user is not None:
        request.session['gauth'] = json.dumps(asdict(user), cls=DjangoJSONEncoder)
        request.session.save()

    return user


def logout(request):
    request.session.flush()


def register(request, **credentials):
    if request.user.is_authenticated:
        logout(request)

    user = PersonManager.create_user(**credentials)

    if user is not None:
        request.session['gauth'] = json.dumps(asdict(user), cls=DjangoJSONEncoder)
        request.session.save()

    return user
