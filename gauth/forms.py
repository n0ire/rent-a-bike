from django import forms
from enum import Enum


class RolesEnum(Enum):
    ADMIN = 'admin'
    PETUGAS = 'petugas'
    ANGGOTA = 'anggota'

    @classmethod
    def choices(cls):
        return tuple((x.name, x.value) for x in cls)


ktp_field = forms.CharField(max_length=20)
email_field = forms.EmailField(max_length=50)
nama_field = forms.CharField(strip=True, max_length=50)
tgl_lahir_field = forms.DateField()
no_telp_field = forms.CharField(required=False, max_length=20)
alamat_field = forms.CharField(required=False, widget=forms.Textarea)
role_field = forms.ChoiceField(choices=RolesEnum.choices())


class LoginForm(forms.Form):
    ktp = ktp_field
    email = email_field


class RegisterForm(forms.Form):
    ktp = ktp_field
    nama = nama_field
    email = email_field
    tgl_lahir = tgl_lahir_field
    no_telp = no_telp_field
    alamat = alamat_field
    role = role_field
