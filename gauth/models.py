import dataclasses
from datetime import date, datetime
from django.db import connection
from django.db.utils import IntegrityError

SCHEMA = 'BIKE_SHARING'


def fetchall(datacls, cursor):
    "Return all rows from a cursor as list of dataclass instances"
    columns = [col[0] for col in cursor.description]
    return [
        datacls(**dict(zip(columns, row)))
        for row in cursor.fetchall()
    ]


def fetchone(datacls, cursor):
    "Return one row from a cursor as a dataclass instance"
    columns = [col[0] for col in cursor.description]
    fetched = cursor.fetchone()

    if fetched:
        return datacls(**dict(zip(columns, fetched)))


def construct_and_query(datacls, table_name, attrs):
    conditions = []
    params = []

    for attr in attrs:
        filtered = filter(lambda f: f.name == attr,
                          dataclasses.fields(datacls))
        filtered = list(filtered)[0]
        field, typ = filtered.name, filtered.type

        if not isinstance(attrs[field], typ):
            # TODO raise error
            pass

        template = f"{field} = %s"

        conditions.append(template)
        params.append(attrs[attr])

    conditions = ' AND '.join(conditions) or ' TRUE'
    query = "SELECT * FROM %s WHERE %s;" % (table_name, conditions)
    return query, params


def construct_single_insert(datacls, table_name, attrs):
    obj = datacls(**attrs)
    attrs = dataclasses.asdict(obj)
    values = ",".join("%s" for i in range(len(attrs)))
    query = "INSERT INTO %s values (%s);" % (table_name, values)

    return query, list(attrs.values())


@dataclasses.dataclass
class Person:
    ktp: str = None
    email: str = None
    nama: str = None
    alamat: str = None
    tgl_lahir: date = None
    no_telp: str = None

    @property
    def anggota(self):
        if not self.is_anggota:
            return None
        elif hasattr(self, '_anggota'):
            return self._anggota
        else:
            self._anggota = AnggotaManager.get(ktp=self.ktp)
            return self._anggota

    @property
    def petugas(self):
        if not self.is_petugas:
            return None
        elif hasattr(self, '_petugas'):
            return self._petugas
        else:
            self._petugas = PetugasManager.get(ktp=self.ktp)
            return self._petugas

    @property
    def is_authenticated(self):
        return not self.is_anonymous

    @property
    def is_anonymous(self):
        return False

    @property
    def is_admin(self):
        return False

    @property
    def is_petugas(self):
        return bool(PetugasManager.get(ktp=self.ktp))

    @property
    def is_anggota(self):
        return not (self.is_admin or self.is_petugas)


class AnonymousPerson(Person):
    @property
    def is_anonymous(self):
        return True


class PersonManager:
    TABLE_NAME = 'PERSON'
    model = Person

    @classmethod
    def table_name(cls):
        return "%s.%s" % (SCHEMA, cls.TABLE_NAME)

    @classmethod
    def get(cls, **attrs):
        query, params = construct_and_query(cls.model, cls.table_name(), attrs)

        with connection.cursor() as c:
            c.execute(query, params)
            results = fetchone(cls.model, c)

        return results

    @classmethod
    def filter(cls, **attrs):
        query, params = construct_and_query(cls.model, cls.table_name(), attrs)

        with connection.cursor() as c:
            c.execute(query, params)
            results = fetchall(cls.model, c)

        return results

    @classmethod
    def create_user(cls, **attrs):
        query, params = construct_single_insert(cls.model, cls.table_name(), attrs)

        with connection.cursor() as c:
            c.execute(query, params)

        return cls.model(**attrs)


@dataclasses.dataclass
class Anggota:
    no_kartu: str = None
    saldo: float = None
    points: int = 0
    ktp: str = None


class AnggotaManager:
    TABLE_NAME = 'ANGGOTA'
    model = Anggota

    @classmethod
    def table_name(cls):
        return "%s.%s" % (SCHEMA, cls.TABLE_NAME)

    @classmethod
    def get(cls, **attrs):
        query, params = construct_and_query(cls.model, cls.table_name(), attrs)

        with connection.cursor() as c:
            c.execute(query, params)
            results = fetchone(cls.model, c)

        return results

    @classmethod
    def create(cls, **attrs):
        with connection.cursor() as c:
            c.execute("SELECT MAX(CAST(coalesce(no_kartu, '0') AS integer))"
                      "    FROM BIKE_SHARING.ANGGOTA;")
            max_no_kartu = int(c.fetchone()[0])

            attrs['no_kartu'] = max_no_kartu + 1

            query, params = construct_single_insert(cls.model, cls.table_name(), attrs)

            c.execute(query, params)

        return cls.model(**attrs)


@dataclasses.dataclass
class Petugas:
    ktp: str = None
    gaji: float = None


class PetugasManager:
    TABLE_NAME = 'PETuGAS'
    model = Petugas

    @classmethod
    def table_name(cls):
        return "%s.%s" % (SCHEMA, cls.TABLE_NAME)

    @classmethod
    def get(cls, **attrs):
        query, params = construct_and_query(cls.model, cls.table_name(), attrs)

        with connection.cursor() as c:
            c.execute(query, params)
            results = fetchone(cls.model, c)

        return results

    @classmethod
    def create(cls, **attrs):
        query, params = construct_single_insert(cls.model, cls.table_name(), attrs)

        with connection.cursor() as c:
            c.execute(query, params)

        return cls.model(**attrs)
