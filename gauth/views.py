from django.shortcuts import render, redirect
from gauth import forms, models
from gauth.utils import authenticate, logout, register


# Create your views here.
def login_view(request):
    if request.method == 'POST':
        form = forms.LoginForm(request.POST)

        if form.is_valid():
            ktp = form.cleaned_data['ktp']
            email = form.cleaned_data['email']

            user = authenticate(request, ktp=ktp, email=email)

        return redirect(request.POST.get('next', '/'))

    else:
        form = forms.LoginForm()

    context = {
        'next': request.GET.get('next', '/'),
        'form': form
    }

    return render(request, 'login.html', context)


def register_view(request):
    if request.method == 'POST':
        form = forms.RegisterForm(request.POST)

        if form.is_valid():
            ktp = form.cleaned_data['ktp']
            nama = form.cleaned_data['nama']
            email = form.cleaned_data['email']
            tgl_lahir = form.cleaned_data['tgl_lahir']
            no_telp = form.cleaned_data['no_telp']
            alamat = form.cleaned_data['alamat']

            role = form.cleaned_data['role']

            user = register(request, ktp=ktp, nama=nama, email=email,
                            tgl_lahir=tgl_lahir, no_telp=no_telp, alamat=alamat)

            if role == forms.RolesEnum.ANGGOTA.name:
                anggota = models.AnggotaManager.create(saldo=0.0, points=0, ktp=ktp)
            else:
                petugas = models.PetugasManager.create(ktp=ktp, gaji=30000.0)

        return redirect('/')

    else:
        form = forms.RegisterForm()

    context = {
        'form': form
    }

    return render(request, 'register.html', context)


def logout_view(request):
    logout(request)
    return redirect('login')
