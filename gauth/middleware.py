from django.conf import settings
from gauth.models import AnonymousPerson, Person
from gauth.utils import authenticate
from importlib import import_module
import json

SessionStore = import_module(settings.SESSION_ENGINE).SessionStore


class GauthMiddleware:
    def __init__(self, get_response):
        self.get_response = get_response
        # One-time configuration and initialization.

    def __call__(self, request):
        # Code to be executed for each request before
        # the view (and later middleware) are called.

        if 'gauth' in request.session.keys():
            creds = json.loads(request.session.get('gauth'))
            request.user = Person(**creds)
        else:
            request.user = AnonymousPerson()

        response = self.get_response(request)

        # Code to be executed for each request/response after
        # the view is called.

        return response
