from django.db import models
from django.db import connection

# Create your models here.
SCHEMA = "bike_sharing"

PERSON = SCHEMA + "." + "person"
ANGGOTA = SCHEMA + "." + "anggota"
VOUCHER = SCHEMA + "." + "voucher"

cursor =  connection.cursor()

class Voucher:

    def get_all_voucher():
        query = 'select * from %s ;' % (voucher)
        cursor.execute(query)
        return cursor

    def get_voucher_by_id(id):
        query = 'select * from %s where id_voucher = \'%d\' ;' % (VOUCHER, id)
        cursor.execute(query)
        return cursor

    def get_voucher_and_join_anggota_pengklaim():
        query = '''select CAST (v.id_voucher AS INTEGER) as id, v.nama as name,
                    v.kategori as kategori, v.nilai_point as nilai_point,
                    v.deskripsi as deskripsi, v.no_kartu_anggota as no_kartu_anggota, p.nama as pengklaim
                    from %s as v,
                    %s as a,
                    %s as p
                    where v.no_kartu_anggota = a.no_kartu
                    and a.ktp = p.ktp
                    order by id asc
                    ;
                ''' % (VOUCHER, ANGGOTA, PERSON)
        cursor.execute(query)
        return cursor
