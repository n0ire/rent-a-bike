# Create your views here.

from django.shortcuts import render
from .models import Voucher

def get_as_dict(cursor):
    columns = [column[0] for column in cursor.description]
    results = []
    for row in cursor.fetchall():
        results.append(dict(zip(columns, row)))
    return results

def index(request):
    context = {
        "vouchers" : get_as_dict(Voucher.get_voucher_and_join_anggota_pengklaim())
    }
    # print(len(context["sepedas"]))
    # print(context["sepedas"])
    return render(request, 'daftar_voucher.html', context)

def edit(request, id):
    context = {
        "vouchers" : get_as_dict(Voucher.get_voucher_by_id(id))
    }
    return render(request, 'update_voucher.html', context)

def create(request):
    return render(request, 'form_voucher.html')

