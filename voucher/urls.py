from django.urls import path
from django.conf.urls import url, include
from .views import *

#url for app

urlpatterns = [
    path('', index, name = "index"),
    path('form-voucher', create, name = "form_voucher"),
    path('edit/<int:id>', edit, name = "update_voucher")
]
