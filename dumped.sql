--
-- PostgreSQL database dump
--

-- Dumped from database version 9.4.22
-- Dumped by pg_dump version 11.2

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET client_min_messages = warning;
SET row_security = off;

--
-- Name: bike_sharing; Type: SCHEMA; Schema: -; Owner: -
--

CREATE SCHEMA bike_sharing;


--
-- Name: add_point_anggota(); Type: FUNCTION; Schema: bike_sharing; Owner: -
--

CREATE FUNCTION bike_sharing.add_point_anggota() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
    BEGIN
        IF(new.denda = 0) THEN
            UPDATE anggota a
            SET points = a.points + 1000
            WHERE a.no_kartu  = NEW.no_kartu_anggota;
            RETURN NEW;
        END IF;
    END;
$$;


--
-- Name: hitung_kolom_denda(); Type: FUNCTION; Schema: bike_sharing; Owner: -
--

CREATE FUNCTION bike_sharing.hitung_kolom_denda() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
declare
temp record;
begin
for temp in
select no_kartu_anggota as NK, EXTRACT(EPOCH FROM(datetime_kembali - datetime_pinjam)) as duration
from peminjaman
loop
update peminjaman
set peminjaman.denda = 50000*(temps.durasi - 10)
where peminjaman.no_kartu_anggota = temp.NK and temps.duration > 36000 and temps.duration < 86400;
end loop;
return new;
end;
$$;


--
-- Name: insert_transaksi_khusus_peminjaman(); Type: FUNCTION; Schema: bike_sharing; Owner: -
--

CREATE FUNCTION bike_sharing.insert_transaksi_khusus_peminjaman() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
begin
if (TG_OP='UPDATE') and NEW.biaya is not null then insert into transaksi
(no_kartu_anggota, date_time, jenis, nominal) VALUES(OLD.no_kartu_anggota, NEW.datetime_kembali,
'Peminjaman', NEW.biaya + NEW.denda);
insert into TRANSAKSI_KHUSUS_PEMINJAMAN(no_kartu_anggota, date_time, datetime_pinjam, no_sepeda,
id_stasiun, no_kartu_peminjam) VALUES(OLD.no_kartu_anggota, NEW.datetime_kembali,
OLD.datetime_pinjam, OLD.nomor_sepeda, OLD.id_stasiun, OLD.no_kartu_anggota);
end if;
return NEW;
end;
$$;


--
-- Name: insert_transaksi_peminjaman(); Type: FUNCTION; Schema: bike_sharing; Owner: -
--

CREATE FUNCTION bike_sharing.insert_transaksi_peminjaman() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
  BEGIN
    IF OLD.datetime_kembali IS NULL AND
       NEW.datetime_kembali IS NOT NULL THEN
      INSERT INTO TRANSAKSI VALUES 
        (NEW.no_kartu_anggota, NEW.datetime_kembali, 'peminjaman', NEW.biaya + NEW.denda);
      INSERT INTO TRANSAKSI_KHUSUS_PEMINJAMAN VALUES 
        (NEW.no_kartu_anggota, NEW.datetime_kembali, NEW.no_kartu_anggota, NEW.datetime_pinjam, NEW.no_sepeda, NEW.id_stasiun);
    END IF;
    RETURN NEW;
  END;
$$;


--
-- Name: laporan_lebih_24_jam(); Type: FUNCTION; Schema: bike_sharing; Owner: -
--

CREATE FUNCTION bike_sharing.laporan_lebih_24_jam() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
declare
counter int;
hour int;
begin
select count(*) - 1
into counter
from laporan;
hour:=CEILING(EXTRACT(EPOCH FROM NEW.datetime_kembali - OLD.datetime_pinjam)/3600);
if hour>24 then
insert into laporan values
(counter, NEW.no_kartu_anggota, NEW.datetime_pinjam, NEW.nomor_sepeda, NEW.id_stasiun, 'masuk');
end if;
return new;
end
$$;


--
-- Name: subtract_point_anggota(); Type: FUNCTION; Schema: bike_sharing; Owner: -
--

CREATE FUNCTION bike_sharing.subtract_point_anggota() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
    BEGIN
        UPDATE anggota a
        SET points = a.points - new.nilai_point
        WHERE a.no_kartu  = NEW.no_kartu_anggota;
        RETURN NEW;
    END;
$$;


--
-- Name: subtract_points_anggota(); Type: FUNCTION; Schema: bike_sharing; Owner: -
--

CREATE FUNCTION bike_sharing.subtract_points_anggota() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
    BEGIN
        UPDATE anggota a
        SET points = a.points - new.nilai_point
        WHERE a.no_kartu  = NEW.no_kartu_anggota;
        RETURN NEW;
    END;
$$;


--
-- Name: update_saldo_anggota(); Type: FUNCTION; Schema: bike_sharing; Owner: -
--

CREATE FUNCTION bike_sharing.update_saldo_anggota() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
    BEGIN
        IF(new.jenis = 'TopUp') THEN
            UPDATE anggota a
            SET saldo = a.saldo + new.nominal
            WHERE a.no_kartu  = NEW.no_kartu_anggota;
            RETURN NEW;

        ELSE
            UPDATE anggota a
            SET saldo = a.saldo - new.nominal
            WHERE a.no_kartu  = NEW.no_kartu_anggota;
            RETURN NEW;
        END IF;
    END;
$$;


SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: acara; Type: TABLE; Schema: bike_sharing; Owner: -
--

CREATE TABLE bike_sharing.acara (
    id_acara character varying(10) NOT NULL,
    judul character varying(100) NOT NULL,
    deskripsi text,
    tgl_mulai date NOT NULL,
    tgl_akhir date NOT NULL,
    is_free boolean NOT NULL
);


--
-- Name: acara_stasiun; Type: TABLE; Schema: bike_sharing; Owner: -
--

CREATE TABLE bike_sharing.acara_stasiun (
    id_stasiun character varying(10) NOT NULL,
    id_acara character varying(10) NOT NULL
);


--
-- Name: anggota; Type: TABLE; Schema: bike_sharing; Owner: -
--

CREATE TABLE bike_sharing.anggota (
    no_kartu character varying(10) NOT NULL,
    saldo real,
    points integer,
    ktp character varying(20) NOT NULL
);


--
-- Name: laporan; Type: TABLE; Schema: bike_sharing; Owner: -
--

CREATE TABLE bike_sharing.laporan (
    id_laporan character varying(10) NOT NULL,
    no_kartu_anggota character varying(10) NOT NULL,
    datetime_pinjam timestamp without time zone NOT NULL,
    nomor_sepeda character varying(10) NOT NULL,
    id_stasiun character varying(10) NOT NULL,
    status character varying(20) NOT NULL
);


--
-- Name: peminjaman; Type: TABLE; Schema: bike_sharing; Owner: -
--

CREATE TABLE bike_sharing.peminjaman (
    no_kartu_anggota character varying(10) NOT NULL,
    datetime_pinjam timestamp without time zone NOT NULL,
    nomor_sepeda character varying(10) NOT NULL,
    id_stasiun character varying(10) NOT NULL,
    datetime_kembali timestamp without time zone,
    biaya real,
    denda real
);


--
-- Name: penugasan; Type: TABLE; Schema: bike_sharing; Owner: -
--

CREATE TABLE bike_sharing.penugasan (
    ktp character varying(20) NOT NULL,
    start_datetime timestamp without time zone NOT NULL,
    id_stasiun character varying(10) NOT NULL,
    end_datetime timestamp without time zone NOT NULL
);


--
-- Name: person; Type: TABLE; Schema: bike_sharing; Owner: -
--

CREATE TABLE bike_sharing.person (
    ktp character varying(20) NOT NULL,
    email character varying(50) NOT NULL,
    nama character varying(50) NOT NULL,
    alamat text,
    tgl_lahir date NOT NULL,
    no_telp character varying(20)
);


--
-- Name: petugas; Type: TABLE; Schema: bike_sharing; Owner: -
--

CREATE TABLE bike_sharing.petugas (
    ktp character varying(20) NOT NULL,
    gaji real NOT NULL
);


--
-- Name: sepeda; Type: TABLE; Schema: bike_sharing; Owner: -
--

CREATE TABLE bike_sharing.sepeda (
    nomor character varying(10) NOT NULL,
    merk character varying(10) NOT NULL,
    jenis character varying(50) NOT NULL,
    status boolean NOT NULL,
    id_stasiun character varying(10) NOT NULL,
    no_kartu_penyumbang character varying(20)
);


--
-- Name: stasiun; Type: TABLE; Schema: bike_sharing; Owner: -
--

CREATE TABLE bike_sharing.stasiun (
    id_stasiun character varying(10) NOT NULL,
    alamat text NOT NULL,
    lat real,
    long real,
    nama character varying(50) NOT NULL
);


--
-- Name: transaksi; Type: TABLE; Schema: bike_sharing; Owner: -
--

CREATE TABLE bike_sharing.transaksi (
    no_kartu_anggota character varying(10) NOT NULL,
    date_time timestamp without time zone NOT NULL,
    jenis character varying(20) NOT NULL,
    nominal real NOT NULL
);


--
-- Name: transaksi_khusus_peminjaman; Type: TABLE; Schema: bike_sharing; Owner: -
--

CREATE TABLE bike_sharing.transaksi_khusus_peminjaman (
    no_kartu_anggota character varying(10) NOT NULL,
    date_time timestamp without time zone NOT NULL,
    no_kartu_peminjam character varying(10) NOT NULL,
    datetime_pinjam timestamp without time zone NOT NULL,
    no_sepeda character varying(10) NOT NULL,
    id_stasiun character varying(10) NOT NULL
);


--
-- Name: voucher; Type: TABLE; Schema: bike_sharing; Owner: -
--

CREATE TABLE bike_sharing.voucher (
    id_voucher character varying(10) NOT NULL,
    nama character varying(255) NOT NULL,
    kategori character varying(255) NOT NULL,
    nilai_point real NOT NULL,
    deskripsi text,
    no_kartu_anggota character varying(10) NOT NULL
);


--
-- Data for Name: acara; Type: TABLE DATA; Schema: bike_sharing; Owner: -
--

COPY bike_sharing.acara (id_acara, judul, deskripsi, tgl_mulai, tgl_akhir, is_free) FROM stdin;
1	natoque penatibus et	ut,	2018-08-12	2020-12-22	t
2	Etiam	Praesent	2018-07-15	2021-04-19	f
3	ac mi	parturient montes, nascetur ridiculus mus. Proin vel	2018-07-06	2020-12-09	f
4	sem elit,	mauris sapien, cursus in, hendrerit consectetuer,	2018-04-27	2020-12-21	t
5	vel lectus. Cum sociis	dolor. Quisque tincidunt pede ac urna.	2018-12-17	2020-04-25	f
6	Phasellus	ac ipsum. Phasellus vitae mauris sit amet lorem	2018-08-16	2020-09-27	f
7	hendrerit a, arcu. Sed	at risus. Nunc ac sem ut dolor dapibus	2018-10-27	2020-05-25	t
8	ornare.	non nisi. Aenean eget metus. In nec	2018-08-27	2020-12-28	f
9	metus vitae velit egestas	commodo ipsum. Suspendisse non leo. Vivamus	2018-10-27	2020-07-31	f
10	tellus.	et, eros. Proin ultrices.	2019-01-13	2021-01-16	f
\.


--
-- Data for Name: acara_stasiun; Type: TABLE DATA; Schema: bike_sharing; Owner: -
--

COPY bike_sharing.acara_stasiun (id_stasiun, id_acara) FROM stdin;
2	7
4	6
20	4
9	3
11	6
12	1
6	4
9	7
5	1
14	9
3	8
15	4
9	5
17	7
7	10
14	10
7	6
4	5
7	4
3	1
20	8
17	8
2	4
16	6
17	9
\.


--
-- Data for Name: anggota; Type: TABLE DATA; Schema: bike_sharing; Owner: -
--

COPY bike_sharing.anggota (no_kartu, saldo, points, ktp) FROM stdin;
2	314894	798	2398288953604667392
3	209169	752	7480185113633979392
4	385866	779	3743371030204085760
6	57146	949	7528412585231975424
7	832485	567	3668130809658200320
8	30576	904	5110714773147097088
9	889529	70	6138081529755218432
10	755459	211	2697282799203850880
11	797814	449	2158873416245635584
12	612315	646	6605903777499951104
13	662391	966	4354891782426821120
14	42056	157	4137226232103616000
15	680395	999	3490557993941961728
16	623370	170	2209235808984362368
18	835333	428	3483914043921419008
19	165891	386	6106222290313469952
20	317075	882	5894111260194926080
21	527515	301	3396351974145254400
22	775183	589	3451915547787178752
23	622595	871	5986662410083079680
24	971892	169	7568270814380941312
25	672204	412	3732614571087097344
26	48150	209	5020516350511507456
27	679478	319	6078500302311968768
28	769561	310	4730719149044237312
29	785148	665	1762552532160579168
30	141765	238	3187185016767923456
31	717024	225	5843103683067199488
32	104988	919	2662462290050408192
33	606147	882	2216821090617226624
34	854399	774	2767780694754734080
35	734199	597	5248207020013582336
36	744479	502	1733577508777290816
37	757830	871	6573871413980708864
38	687951	495	2135470740747375744
39	677069	20	7761864040216888320
40	30613	789	5805431054623553536
41	994567	867	2918660036704350464
42	489012	266	3631906878426198784
43	956379	411	4864010008797254656
44	494793	776	2364904536144634496
45	292578	523	5410840236688601600
46	645999	496	7221533766649950208
47	252771	74	5470867179286358528
48	170997	299	6906116331082744832
49	204852	854	3994747922695836672
50	985690	738	4560738494906321408
51	22246	197	1572738794814717188
52	439847	73	7452288549186347008
53	670546	540	5413626047538956288
54	455873	806	5370125283633362432
55	635328	272	5783668331877084160
56	636902	763	4070480501996178432
57	641071	347	3996105812149659648
58	248141	848	1802790301109776512
59	740637	833	7548617292541761536
60	963603	192	4373597408063164416
61	393080	77	2377318471911496192
62	487766	208	4589892555995579904
63	953428	246	4404461670834269696
64	257221	681	4411435901634280960
65	706440	387	5863466093887072768
66	298120	820	5663980654779248640
67	39786	287	6313063183843343360
68	773500	797	1753525332000767232
69	433161	413	4313919314575385600
70	599822	415	2521054185379633408
1	671224	394	4537200945859963904
5	919512	1225	1585576122077904160
17	100542	1057	3016205214015987200
\.


--
-- Data for Name: laporan; Type: TABLE DATA; Schema: bike_sharing; Owner: -
--

COPY bike_sharing.laporan (id_laporan, no_kartu_anggota, datetime_pinjam, nomor_sepeda, id_stasiun, status) FROM stdin;
1	45	2019-04-09 07:13:57	13	10	Sudah Selesai
2	13	2019-04-11 06:18:07	19	3	Sudah Selesai
3	34	2019-04-10 12:29:01	8	6	Sudah Selesai
4	14	2019-04-11 18:25:52	39	1	Sudah Selesai
5	57	2019-04-11 19:10:02	6	17	Sudah Selesai
6	24	2019-04-10 07:21:10	11	1	Sudah Selesai
7	56	2019-04-11 09:25:40	42	8	Sudah Selesai
8	28	2019-04-11 18:07:44	50	6	Sudah Selesai
9	9	2019-04-11 17:28:26	18	2	Sudah Selesai
10	18	2019-04-09 08:38:08	1	15	Sudah Selesai
\.


--
-- Data for Name: peminjaman; Type: TABLE DATA; Schema: bike_sharing; Owner: -
--

COPY bike_sharing.peminjaman (no_kartu_anggota, datetime_pinjam, nomor_sepeda, id_stasiun, datetime_kembali, biaya, denda) FROM stdin;
5	2019-04-09 18:24:29	27	4	2019-04-09 19:24:07	1000	0
30	2019-04-09 18:59:57	36	15	2019-04-09 22:51:22	4000	0
67	2019-04-09 20:01:55	25	4	2019-04-10 07:14:47	10000	100000
6	2019-04-09 07:09:44	9	10	2019-04-09 09:55:11	3000	0
26	2019-04-09 08:16:22	4	15	2019-04-09 14:39:50	7000	0
47	2019-04-09 15:16:03	35	6	2019-04-09 20:20:15	6000	0
39	2019-04-09 15:39:34	24	6	2019-04-09 18:19:02	3000	0
15	2019-04-09 20:27:36	19	4	2019-04-10 16:16:18	10000	500000
55	2019-04-09 21:14:15	44	4	2019-04-10 08:31:49	10000	100000
13	2019-04-09 20:37:33	50	10	2019-04-09 21:04:26	1000	0
45	2019-04-09 07:13:57	13	10	2019-04-10 18:10:36	10000	1200000
18	2019-04-09 08:38:08	1	15	2019-04-10 22:33:20	10000	1200000
58	2019-04-09 14:20:40	43	7	2019-04-09 22:14:40	5000	0
24	2019-04-09 13:25:22	22	15	2019-04-09 18:54:08	6000	0
13	2019-04-11 06:18:07	19	3	2019-04-12 21:29:49	10000	1200000
42	2019-04-11 19:08:32	36	14	2019-04-11 20:16:17	2000	0
52	2019-04-10 13:22:23	48	12	2019-04-10 16:40:47	4000	0
7	2019-04-11 10:16:24	29	9	2019-04-11 18:26:12	9000	0
70	2019-04-11 09:57:41	27	16	2019-04-11 16:18:06	7000	0
38	2019-04-11 11:13:18	2	7	2019-04-11 16:28:06	6000	0
33	2019-04-11 14:15:50	17	2	2019-04-11 18:53:33	5000	0
34	2019-04-10 12:29:01	8	6	2019-04-11 23:08:46	10000	1200000
22	2019-04-10 15:29:17	49	20	2019-04-10 19:58:13	5000	0
14	2019-04-11 18:25:52	39	1	2019-04-13 21:37:34	10000	1200000
67	2019-04-10 19:30:50	34	2	2019-04-10 23:40:44	5000	0
26	2019-04-11 07:27:15	48	5	2019-04-11 22:35:03	10000	300000
39	2019-04-11 08:50:22	47	4	2019-04-11 13:42:51	5000	0
11	2019-04-11 07:17:15	40	17	2019-04-11 07:57:22	1000	0
57	2019-04-11 19:10:02	6	17	2019-04-13 22:47:26	10000	1200000
24	2019-04-10 07:21:10	11	1	2019-04-12 21:11:41	10000	1200000
53	2019-04-11 09:48:36	49	18	2019-04-11 10:21:32	1000	0
57	2019-04-10 15:49:44	12	12	2019-04-10 23:37:11	8000	0
62	2019-04-11 11:36:21	50	20	2019-04-11 11:49:23	1000	0
9	2019-04-11 17:28:26	18	2	2019-04-13 20:09:32	10000	1200000
49	2019-04-10 13:36:57	48	10	2019-04-10 20:49:10	8000	0
36	2019-04-11 08:28:10	34	13	2019-04-11 13:47:53	6000	0
8	2019-04-10 11:45:34	30	15	2019-04-10 13:18:37	2000	0
3	2019-04-11 09:44:10	28	17	2019-04-11 15:47:26	7000	0
56	2019-04-11 09:25:40	42	8	2019-04-12 21:27:15	10000	1200000
32	2019-04-10 13:00:26	35	15	2019-04-10 18:37:23	6000	0
2	2019-04-11 13:45:26	9	1	2019-04-11 18:36:18	5000	0
31	2019-04-10 12:45:20	31	3	2019-04-10 13:40:39	1000	0
10	2019-04-10 08:44:07	43	8	2019-04-10 11:44:05	3000	0
28	2019-04-11 18:07:44	50	6	2019-04-13 20:20:35	10000	1200000
66	2019-04-10 15:52:01	3	8	2019-04-10 18:09:34	3000	0
48	2019-04-10 15:24:39	43	6	2019-04-10 21:10:59	6000	0
29	2019-04-11 12:41:37	20	15	2019-04-11 22:20:33	10000	0
12	2019-04-10 14:34:13	47	8	2019-04-10 19:39:55	6000	0
30	2019-04-11 11:30:21	36	10	2019-04-11 20:38:44	10000	0
19	2019-04-10 20:51:57	23	8	2019-04-12 21:59:50	10000	1200000
6	2019-04-11 14:36:47	49	17	2019-04-11 20:31:53	6000	0
18	2019-04-11 14:21:48	36	4	2019-04-12 20:06:01	6000	0
54	2019-04-11 19:14:40	29	7	2019-04-12 19:26:31	1000	0
17	2019-04-11 18:10:03	37	17	2019-04-12 16:25:50	10000	350000
18	2019-04-11 09:41:59	24	14	2019-04-11 21:38:24	10000	100000
47	2019-04-11 16:38:17	34	6	2019-04-11 21:51:08	6000	0
20	2019-04-11 14:24:59	32	11	2019-04-11 20:53:38	7000	0
37	2019-04-11 20:58:58	36	11	2019-04-11 21:27:18	1000	0
36	2019-04-11 15:16:36	28	19	2019-04-11 18:02:25	3000	0
8	2019-04-11 07:51:34	48	3	2019-04-13 13:59:44	7000	0
40	2019-04-11 21:54:31	46	19	2019-04-11 23:21:53	2000	0
70	2019-04-11 19:43:37	46	16	2019-04-12 11:50:09	10000	250000
43	2019-04-11 10:38:49	35	6	2019-04-11 14:42:59	5000	0
33	2019-04-11 15:25:31	5	2	2019-04-11 20:28:08	6000	0
21	2019-04-11 13:08:30	9	3	2019-04-12 15:01:14	10000	800000
44	2019-04-12 14:02:31	18	20	2019-04-12 23:30:56	10000	0
25	2019-04-12 18:42:25	38	19	2019-04-12 23:36:06	5000	0
10	2019-04-12 08:42:43	18	3	2019-04-12 11:40:32	3000	0
29	2019-04-12 17:11:35	1	1	2019-04-12 22:51:22	6000	0
31	2019-04-12 06:52:48	5	5	2019-04-13 13:44:14	10000	1050000
15	2019-04-12 15:19:26	27	19	2019-04-13 08:18:56	10000	350000
37	2019-04-12 10:57:36	24	11	2019-04-13 22:48:24	10000	1200000
34	2019-04-12 07:06:48	10	10	2019-04-12 17:36:20	10000	50000
41	2019-04-12 06:52:42	6	7	2019-04-12 08:39:53	2000	0
67	2019-04-11 07:36:20	13	19	2019-04-11 11:17:12	4000	0
23	2019-04-11 22:41:08	1	4	2019-04-11 10:22:01	10000	100000
5	2019-04-07 04:30:30	2	1	2019-04-07 04:32:30	10000	0
5	2019-04-07 04:30:32	2	1	2019-04-07 04:32:31	10000	0
5	2019-04-07 04:30:33	2	1	2019-04-07 04:32:32	10000	0
5	2019-04-07 04:30:34	2	1	2019-04-07 04:32:34	10000	0
17	2019-05-07 07:41:08	1	4	\N	\N	0
\.


--
-- Data for Name: penugasan; Type: TABLE DATA; Schema: bike_sharing; Owner: -
--

COPY bike_sharing.penugasan (ktp, start_datetime, id_stasiun, end_datetime) FROM stdin;
2572502157132137216	2019-04-09 19:04:19	8	2019-04-11 20:32:43
2237049981890043008	2019-04-09 11:50:27	9	2019-04-12 09:03:18
4049985688390790144	2019-04-10 16:31:18	10	2019-04-12 13:12:55
4661293772762051584	2019-04-10 16:14:08	19	2019-04-10 22:35:38
3859658284133070336	2019-04-09 12:23:01	9	2019-04-10 10:00:57
2720285977852354048	2019-04-10 00:42:06	5	2019-04-11 22:40:16
5530881750712214528	2019-04-10 09:01:48	12	2019-04-13 06:27:13
1956731690374177792	2019-04-11 22:46:08	11	2019-04-13 04:25:55
7692938128301457408	2019-04-11 03:46:27	16	2019-04-12 06:07:54
5298816245984740352	2019-04-10 03:50:18	11	2019-04-13 16:37:37
4985566861186291200	2019-04-09 22:42:41	3	2019-04-13 16:15:54
2816947785211598592	2019-04-11 14:24:43	18	2019-04-13 19:53:37
7690336577319205888	2019-04-11 06:37:50	16	2019-04-13 15:33:47
6917740149379137536	2019-04-09 13:46:51	5	2019-04-13 04:12:11
3920852170213117440	2019-04-10 19:55:54	3	2019-04-12 10:19:21
2749411521017986816	2019-04-10 08:57:30	17	2019-04-11 23:09:45
4375153913267917824	2019-04-09 17:41:58	16	2019-04-10 11:57:51
2005122009528949184	2019-04-10 01:07:38	6	2019-04-12 22:06:34
5841751611767420928	2019-04-10 06:07:23	18	2019-04-13 01:44:11
4911219887856799232	2019-04-09 07:20:34	18	2019-04-12 06:24:05
5259074154249814528	2019-04-09 19:35:39	10	2019-04-12 23:25:40
7156683117229690880	2019-04-09 22:30:53	7	2019-04-12 11:23:41
3480564731398685184	2019-04-11 17:46:32	19	2019-04-12 23:14:00
4190463562521877504	2019-04-09 08:47:31	13	2019-04-10 16:51:02
4357230393929220608	2019-04-10 09:21:14	10	2019-04-11 18:53:06
5915694870013145088	2019-04-09 10:30:13	19	2019-04-11 12:04:53
7283148029548791808	2019-04-09 01:55:27	3	2019-04-10 23:08:41
4952363457463661568	2019-04-11 03:21:21	12	2019-04-13 13:06:08
4514994758841059328	2019-04-11 17:13:20	8	2019-04-12 15:13:34
7386314486838618112	2019-04-09 08:16:06	11	2019-04-11 02:53:43
\.


--
-- Data for Name: person; Type: TABLE DATA; Schema: bike_sharing; Owner: -
--

COPY bike_sharing.person (ktp, email, nama, alamat, tgl_lahir, no_telp) FROM stdin;
4537200945859963904	convallis.convallis.dolor@aceleifend.co.uk	Josiah Spence	Ap #845-1709 Praesent St.	1987-05-02	(652) 298-5793
2398288953604667392	ornare@magnased.org	Aubrey Bonner	Ap #121-5355 Sed St.	1968-12-06	(422) 429-2066
7480185113633979392	fames@in.net	Jordan Francis	1216 Ac St.	1991-09-01	(515) 690-2430
3743371030204085760	nisi@feugiattelluslorem.edu	Dillon Kelley	P.O. Box 792, 9147 Dignissim Avenue	1990-08-31	(760) 913-9913
1585576122077904160	at.libero.Morbi@dignissimlacusAliquam.edu	Reuben Diaz	758-5799 Pede. Road	1979-07-29	(997) 641-9240
7528412585231975424	elementum.lorem.ut@tinciduntaliquam.org	Hamish Hughes	107-8378 Interdum. Av.	1985-04-29	(746) 886-9189
3668130809658200320	cursus.Integer@ataugueid.com	Alana Mccarty	9949 Mauris Avenue	1957-09-12	(338) 915-3355
5110714773147097088	et.ultrices.posuere@turpisegestasAliquam.ca	Ina Cross	Ap #486-2741 Tellus Rd.	1967-12-14	(732) 196-9122
6138081529755218432	quam@magna.edu	Rashad Griffin	540-9497 Quis Avenue	1966-04-20	(380) 451-7853
2697282799203850880	malesuada@Duismi.co.uk	Curran Douglas	134-8443 Dolor St.	1971-01-06	(730) 156-6286
2158873416245635584	diam@consequatlectus.edu	Madeline Mathews	P.O. Box 250, 1445 Malesuada St.	1956-02-24	(188) 655-4236
6605903777499951104	erat@eu.edu	Odysseus Burt	919-5634 Curae; St.	1989-07-30	(176) 865-8218
4354891782426821120	lectus@Cumsociisnatoque.ca	Lael Buck	101-6582 Eu, Av.	1960-04-05	(762) 817-9175
4137226232103616000	Aliquam@augueidante.edu	Lila Henderson	Ap #177-1455 Facilisis Ave	1955-12-08	(667) 585-8233
3490557993941961728	Etiam.ligula@magnisdisparturient.com	Fleur Hubbard	Ap #297-7084 Sem, St.	1970-09-18	(551) 833-8955
2209235808984362368	Nullam.feugiat.placerat@vitaeodiosagittis.co.uk	Clarke Puckett	Ap #309-6085 Ornare. St.	1988-02-12	(184) 854-9199
3016205214015987200	et.malesuada.fames@Nullatincidunt.com	Vladimir Serrano	527-4929 Nulla Street	1965-07-17	(197) 975-8133
3483914043921419008	molestie@velfaucibusid.ca	Sacha Griffin	P.O. Box 591, 1605 Malesuada St.	1964-04-24	(790) 176-2313
6106222290313469952	sociis@duinecurna.net	Beau Wagner	Ap #770-1694 Phasellus Street	1971-01-20	(753) 698-1645
5894111260194926080	elit@eueleifendnec.com	Kadeem Hays	8860 Sit Av.	1987-05-06	(407) 374-8856
3396351974145254400	in.magna@lectusjusto.edu	Craig Powers	Ap #268-7959 Vel Av.	1969-06-02	(144) 637-8881
3451915547787178752	vel@dictum.edu	Odysseus Zimmerman	516-5471 Proin Road	1984-04-07	(677) 729-6413
5986662410083079680	massa.Suspendisse.eleifend@ametfaucibus.co.uk	Samson May	Ap #163-5342 Integer Avenue	1953-12-30	(103) 667-6003
7568270814380941312	ante.bibendum.ullamcorper@nonegestas.com	Madeson Mathews	779 Nunc Avenue	1979-02-25	(439) 619-7712
3732614571087097344	Sed@ProinmiAliquam.com	Yvonne Grimes	P.O. Box 851, 3464 Luctus Road	1994-05-15	(249) 229-2298
5020516350511507456	dignissim.Maecenas.ornare@atarcuVestibulum.co.uk	Rudyard Snow	422-8958 Mattis. St.	1995-01-05	(801) 746-8458
6078500302311968768	Cras.convallis.convallis@arcuiaculis.edu	Quyn Ratliff	P.O. Box 634, 932 Ipsum. Av.	1978-05-03	(370) 479-8911
4730719149044237312	sed.facilisis@Aeneanegestas.edu	Xaviera Keith	9749 Pellentesque Avenue	1977-01-30	(646) 836-5292
1762552532160579168	aliquet.sem.ut@nisl.org	Leroy Peterson	4567 In St.	1992-03-20	(460) 558-3239
3187185016767923456	nec.urna@cursus.ca	Ivor Cohen	P.O. Box 586, 4909 Fringilla St.	1978-04-16	(695) 904-5145
5843103683067199488	elit.pharetra.ut@variusNam.net	Katell Walker	P.O. Box 779, 6883 Magnis Avenue	1991-11-06	(579) 566-5093
2662462290050408192	Nunc.sollicitudin@mi.com	Erich Gibson	P.O. Box 156, 7032 Nullam St.	1974-02-09	(224) 798-7312
2216821090617226624	Aliquam@Duisacarcu.co.uk	Winifred Torres	3989 Ipsum Rd.	1999-02-04	(648) 440-6497
2767780694754734080	eget.dictum@quisdiam.org	Cole Rios	Ap #578-8573 Cursus Rd.	1964-05-07	(762) 895-3074
5248207020013582336	eleifend.nunc.risus@egestas.net	Maggy Ochoa	P.O. Box 330, 8520 Dictum Rd.	1996-12-23	(811) 929-8060
1733577508777290816	malesuada@eueleifendnec.org	Robert Livingston	Ap #657-4405 Ipsum St.	1993-02-12	(126) 193-6893
6573871413980708864	erat.eget@fringilla.net	Malachi Kelly	P.O. Box 744, 2807 Rhoncus. Street	1963-08-21	(526) 376-9087
2135470740747375744	sit.amet@eleifendnecmalesuada.edu	Lane Kemp	1291 Urna St.	1959-07-14	(172) 729-1467
7761864040216888320	rutrum.urna.nec@quis.org	Lois Benjamin	5647 Sollicitudin Street	1953-07-16	(364) 347-3811
5805431054623553536	dui.augue@dapibusquamquis.net	Jescie Workman	Ap #851-5693 Amet Ave	1997-09-13	(887) 339-5628
2918660036704350464	amet@sedsem.net	Reece Winters	P.O. Box 311, 3677 A, Ave	1986-07-20	(216) 135-1682
3631906878426198784	nisl.arcu@auctornuncnulla.edu	Kirk Emerson	P.O. Box 684, 2848 Phasellus St.	1962-11-17	(699) 816-1001
4864010008797254656	odio.sagittis@Utnec.com	Lars Massey	P.O. Box 221, 4844 Nunc Avenue	1959-12-31	(346) 198-1578
2364904536144634496	sociis.natoque@elementum.ca	Prescott Austin	Ap #201-1132 Nisl Avenue	1981-01-31	(154) 403-6140
5410840236688601600	lacus.pede@ullamcorper.ca	Tiger Chavez	P.O. Box 559, 7818 Tellus Ave	1994-09-10	(111) 306-0442
7221533766649950208	Cum.sociis@iaculisneceleifend.ca	Coby Horton	Ap #770-4438 Donec Road	1974-11-09	(508) 307-9657
5470867179286358528	adipiscing.lacus.Ut@dolor.edu	Burke Wise	3167 Quisque Road	1983-08-02	(158) 193-6689
6906116331082744832	nibh@ac.ca	Vladimir Cruz	P.O. Box 692, 4505 Non, Rd.	1960-06-13	(376) 816-6183
3994747922695836672	dictum.sapien@eutellusPhasellus.ca	Flynn Petersen	Ap #464-2444 Montes, Road	1967-06-18	(672) 577-9221
4560738494906321408	egestas@cursusdiamat.net	Sophia Bass	4431 Vitae, Av.	1978-02-13	(476) 649-0047
1572738794814717188	arcu@telluslorem.net	Halee Maxwell	P.O. Box 362, 5051 Litora Avenue	1966-11-20	(451) 143-3600
7452288549186347008	sociis@ultricesDuisvolutpat.ca	Cassandra Lawson	8190 Praesent St.	1999-03-14	(253) 498-6153
5413626047538956288	rhoncus.id@dolordolortempus.edu	Brenna Powell	P.O. Box 321, 1177 Ipsum. Avenue	1991-11-27	(719) 962-6669
5370125283633362432	congue.In@eu.edu	David Stone	P.O. Box 386, 6025 Arcu Street	1963-08-17	(833) 465-7708
5783668331877084160	cursus@pedeSuspendisse.net	Moana England	P.O. Box 709, 6237 Sit St.	1962-01-25	(901) 657-6752
4070480501996178432	Ut.semper.pretium@CurabiturdictumPhasellus.org	Deborah Cash	Ap #878-1441 Sem. Av.	1972-06-03	(889) 868-5715
3996105812149659648	morbi.tristique.senectus@nequeet.com	Harriet Castillo	540-4942 Dictum St.	1973-10-05	(303) 909-8382
1802790301109776512	sodales@fames.org	Lee Melendez	171-6428 Duis Ave	1969-05-14	(549) 553-2774
7548617292541761536	tellus.imperdiet.non@facilisis.edu	Camden Mcfadden	2510 Condimentum. St.	1953-01-22	(629) 666-3014
4373597408063164416	eu.elit.Nulla@liberoest.net	Hayden Rivera	4912 Quisque Rd.	1988-03-28	(243) 101-6979
2377318471911496192	eget.tincidunt@Vivamusrhoncus.edu	Aretha Mcintosh	342-3221 Ornare, Avenue	1996-01-19	(475) 939-3559
4589892555995579904	lacus.Mauris.non@velitSedmalesuada.co.uk	Jack Sheppard	827-472 A Street	1961-11-04	(786) 958-4744
4404461670834269696	amet@arcuac.ca	Troy Whitney	2428 In, St.	1955-02-15	(666) 723-0351
4411435901634280960	felis@ut.co.uk	Noble Weiss	4526 Cum Ave	1969-04-06	(724) 876-9223
5863466093887072768	sit@a.co.uk	Hector Wheeler	6112 Euismod Rd.	1984-08-16	(179) 672-6575
5663980654779248640	magna.Sed@mollis.ca	Aquila Reyes	619-7436 Congue Av.	1988-05-08	(446) 430-5745
6313063183843343360	Suspendisse.dui.Fusce@malesuada.net	Cheryl Glover	8143 Praesent Street	1987-03-05	(831) 150-7300
1753525332000767232	quis@adui.edu	Caldwell Bates	990-5027 Nec, Rd.	1991-12-21	(333) 738-3662
4313919314575385600	eu.tempor@Morbimetus.net	Vladimir Wilson	Ap #653-3671 Nibh. Rd.	1966-03-15	(500) 963-4050
2521054185379633408	Lorem.ipsum.dolor@Nullamlobortisquam.org	Yvonne Fleming	Ap #845-4120 Sem Road	1991-11-01	(703) 302-3558
2572502157132137216	Mauris.non.dui@lacusQuisquepurus.org	Carter Gallegos	419-7908 Sociis Avenue	1951-09-17	(938) 314-1999
2237049981890043008	turpis@porttitortellus.com	Stone Cole	Ap #498-1585 Suspendisse Rd.	1974-03-06	(617) 866-2700
4049985688390790144	tincidunt@Sedcongueelit.org	Veda Welch	747-8898 Arcu. St.	1957-07-24	(971) 808-0866
4661293772762051584	amet.nulla@duisemperet.co.uk	Rashad Talley	P.O. Box 481, 8848 Integer Av.	1956-11-22	(366) 146-6931
3859658284133070336	quis@velesttempor.org	Barrett Workman	Ap #762-5202 Felis, Street	1997-01-16	(585) 307-1689
2720285977852354048	gravida.Aliquam@seddictum.ca	Hyacinth Cherry	P.O. Box 847, 5542 Tincidunt, Av.	1968-08-04	(946) 667-5002
5530881750712214528	Sed.nec.metus@egetmassa.org	Juliet Wall	P.O. Box 798, 4203 Lectus Rd.	1963-05-30	(471) 364-3942
1956731690374177792	Integer.urna.Vivamus@pharetranibhAliquam.net	Yoshio Luna	P.O. Box 946, 9180 Velit Rd.	1963-01-16	(856) 987-9259
7692938128301457408	ultrices.sit.amet@luctusvulputate.net	Kasimir Irwin	168-9266 Non, Rd.	1950-12-19	(646) 346-2905
5298816245984740352	ornare@pretium.edu	Rhona Miller	3622 Eu Rd.	1956-12-27	(977) 227-9255
4985566861186291200	metus@ametlorem.edu	Tamekah Rush	P.O. Box 487, 9958 In St.	1998-04-20	(610) 472-8829
2816947785211598592	egestas.blandit.Nam@vitae.org	Amery Barry	150 Nulla. Rd.	1964-07-19	(369) 227-5676
7690336577319205888	Morbi.sit.amet@erat.com	Donna Hayes	Ap #617-4159 Vitae Av.	1965-05-09	(490) 175-0523
6917740149379137536	tincidunt.congue@velitduisemper.ca	Prescott Bowman	P.O. Box 620, 4515 Mauris Av.	1983-11-29	(780) 938-3706
3920852170213117440	ac.tellus@diamdictumsapien.ca	Iliana Jefferson	P.O. Box 706, 2865 Cras St.	1979-02-04	(323) 247-9223
2749411521017986816	augue.scelerisque.mollis@Craspellentesque.com	Amir Rasmussen	692-1796 Et, Rd.	1976-02-13	(479) 436-9012
4375153913267917824	Nunc.ullamcorper.velit@musAenean.edu	Sade Cobb	Ap #740-1148 At, Rd.	1955-05-08	(723) 456-8279
2005122009528949184	nibh@Fuscefermentum.com	Preston Reilly	6915 Quam, Avenue	1982-12-19	(575) 806-1977
5841751611767420928	fermentum.arcu.Vestibulum@risusDonecegestas.org	Darius Combs	752-8006 Sollicitudin Av.	1976-07-12	(168) 488-6990
4911219887856799232	scelerisque.neque@Vivamus.edu	Hashim Ratliff	583-9096 Eu St.	1996-01-29	(388) 944-6632
5259074154249814528	dui@estacmattis.edu	Rama Garza	Ap #836-2936 Aenean Street	1957-09-21	(470) 775-7889
7156683117229690880	metus@ridiculusmusAenean.net	Brett Hudson	522-4272 Nisl. St.	1960-06-13	(874) 967-3524
3480564731398685184	Nullam.feugiat.placerat@ridiculusmus.ca	Irene Wooten	P.O. Box 121, 4982 Morbi Street	1959-08-17	(990) 667-0755
4190463562521877504	sit.amet.ornare@magna.ca	Rhiannon Shannon	Ap #603-8970 Sem Rd.	1991-12-18	(289) 142-7010
4357230393929220608	vel@infaucibus.org	Tanya Gallagher	Ap #159-2836 Magna Road	1955-12-27	(812) 270-9112
5915694870013145088	nec.urna.suscipit@arcu.ca	Sebastian Juarez	P.O. Box 869, 2783 Sit Rd.	1967-06-07	(856) 479-5397
7283148029548791808	pellentesque@aliquetdiam.net	Rogan Dean	Ap #134-273 Ut, Rd.	1971-01-24	(468) 105-3657
4952363457463661568	sit@anteiaculis.ca	Liberty Richmond	8724 Eget St.	1961-05-31	(689) 130-6706
4514994758841059328	vestibulum.neque.sed@amifringilla.ca	Claire Mcdowell	P.O. Box 444, 4945 Lorem St.	1969-08-18	(933) 355-1733
7386314486838618112	felis.orci.adipiscing@aliquetodio.net	Winifred Morton	2409 At Street	1986-01-07	(112) 294-2532
\.


--
-- Data for Name: petugas; Type: TABLE DATA; Schema: bike_sharing; Owner: -
--

COPY bike_sharing.petugas (ktp, gaji) FROM stdin;
2572502157132137216	7830434
2237049981890043008	5506620
4049985688390790144	7530882
4661293772762051584	8451311
3859658284133070336	7912169
2720285977852354048	5235573
5530881750712214528	8718012
1956731690374177792	8353492
7692938128301457408	7338055
5298816245984740352	8624932
4985566861186291200	7089485
2816947785211598592	7670525
7690336577319205888	8442648
6917740149379137536	5258446
3920852170213117440	6160855
2749411521017986816	7248885
4375153913267917824	8027683
2005122009528949184	5904857
5841751611767420928	7833404
4911219887856799232	5455583
5259074154249814528	8402137
7156683117229690880	5123425
3480564731398685184	7879877
4190463562521877504	7726086
4357230393929220608	7353057
5915694870013145088	7392857
7283148029548791808	6486112
4952363457463661568	8989972
4514994758841059328	6400186
7386314486838618112	7211662
\.


--
-- Data for Name: sepeda; Type: TABLE DATA; Schema: bike_sharing; Owner: -
--

COPY bike_sharing.sepeda (nomor, merk, jenis, status, id_stasiun, no_kartu_penyumbang) FROM stdin;
1	IDATEN	ONTEL	t	8	31
2	POLYGON	ONTEL	f	16	19
3	WIMCYCLE	TANDEM	t	14	16
4	IDATEN	MTB	f	7	70
5	UNITED	TANDEM	f	3	22
6	UNITED	MTB	f	14	19
7	WIMCYCLE	TANDEM	f	5	43
8	WIMCYCLE	BMX	f	12	50
9	UNITED	ONTEL	f	19	2
10	UNITED	MTB	t	10	48
11	WIMCYCLE	ONTEL	t	17	65
12	WIMCYCLE	TANDEM	t	9	62
13	UNITED	ONTEL	t	8	55
14	POLYGON	TANDEM	f	3	59
15	WIMCYCLE	ONTEL	t	17	68
16	IDATEN	ONTEL	f	4	40
17	WIMCYCLE	BMX	f	12	5
18	UNITED	BMX	t	20	10
19	UNITED	BMX	f	12	24
20	POLYGON	BMX	f	1	\N
21	POLYGON	BMX	f	8	67
22	POLYGON	ONTEL	t	8	63
23	POLYGON	ONTEL	t	14	59
24	WIMCYCLE	ONTEL	t	5	67
25	IDATEN	TANDEM	t	13	70
26	IDATEN	MTB	f	18	29
27	POLYGON	TANDEM	t	15	36
28	UNITED	ONTEL	t	11	47
29	UNITED	BMX	f	18	26
30	POLYGON	TANDEM	f	14	42
31	UNITED	ONTEL	t	19	13
32	UNITED	TANDEM	t	2	29
33	UNITED	ONTEL	t	13	14
34	UNITED	ONTEL	f	17	14
35	UNITED	TANDEM	f	18	55
36	POLYGON	BMX	t	6	55
37	IDATEN	TANDEM	f	16	59
38	WIMCYCLE	MTB	t	6	27
39	IDATEN	TANDEM	f	18	19
40	WIMCYCLE	ONTEL	f	18	36
41	POLYGON	TANDEM	t	16	39
42	WIMCYCLE	BMX	t	6	54
43	IDATEN	MTB	t	2	43
44	IDATEN	MTB	t	14	55
45	IDATEN	MTB	t	6	14
46	IDATEN	ONTEL	t	13	18
47	POLYGON	TANDEM	f	15	48
48	POLYGON	BMX	f	19	51
49	WIMCYCLE	TANDEM	f	1	11
50	UNITED	BMX	f	10	3
\.


--
-- Data for Name: stasiun; Type: TABLE DATA; Schema: bike_sharing; Owner: -
--

COPY bike_sharing.stasiun (id_stasiun, alamat, lat, long, nama) FROM stdin;
1	Blue Anchor  Crossroad, 594	183.065613	254.646194	El Paso
2	Union  Hill, 8481	259.752991	130.800766	Houston
3	Walnut Boulevard, 8972	129.231918	143.024872	Detroit
4	Aspen Route, 7672	172.09436	65.0312958	Miami
5	Bellenden  Crossroad, 3387	293.223419	232.309937	Rome
6	South Vale, 4048	339.994965	125.71949	Ontario
7	Blake  Road, 9138	10.5169687	117.858803	Escondido
8	Chandos  Street, 6633	244.940903	60.7852173	Jersey City
9	Birkenhead   Route, 1687	332.022675	355.53952	Charlotte
10	Gavel   Route, 5038	40.9948997	29.8515339	Atlanta
11	Victoria  Rue, 9761	25.2687225	9.36702906e+14	Salem
12	Lake Road, 85	3.98233819	226.586441	Pittsburgh
13	Camdale  Vale, 4213	216.181198	286.052948	Murfreesboro
14	South Drive, 441	52.8219528	268.288055	Cincinnati
15	Bury  Vale, 140	89.1920471	256.97995	Murfreesboro
16	Archery  Drive, 9506	348.978271	190.94072	Zurich
17	Cephas  Walk, 7764	301.661652	90.3165588	Philadelphia
18	Bedford  Grove, 5894	100.95858	284.404968	Salem
19	Camden  Tunnel, 6492	294.615967	121.496376	Anaheim
20	Blean   Lane, 1426	86.8462524	30.5366764	Fullerton
\.


--
-- Data for Name: transaksi; Type: TABLE DATA; Schema: bike_sharing; Owner: -
--

COPY bike_sharing.transaksi (no_kartu_anggota, date_time, jenis, nominal) FROM stdin;
1	2019-04-07 04:30:29	TopUp	1000000
2	2019-04-06 00:09:12	TopUp	1000000
3	2019-04-07 02:16:22	TopUp	1000000
4	2019-04-06 12:33:33	TopUp	1000000
5	2019-04-06 10:36:05	TopUp	1000000
6	2019-04-06 02:34:40	TopUp	1000000
7	2019-04-06 11:02:39	TopUp	1000000
8	2019-04-07 19:20:07	TopUp	1000000
9	2019-04-07 16:24:19	TopUp	1000000
10	2019-04-06 02:05:44	TopUp	1000000
11	2019-04-07 20:24:57	TopUp	1000000
12	2019-04-07 01:34:36	TopUp	1000000
13	2019-04-07 13:05:00	TopUp	1000000
14	2019-04-07 18:07:33	TopUp	1000000
15	2019-04-07 11:57:31	TopUp	1000000
16	2019-04-07 00:19:09	TopUp	1000000
17	2019-04-06 09:30:08	TopUp	1000000
18	2019-04-07 11:52:50	TopUp	1000000
19	2019-04-06 21:10:51	TopUp	1000000
20	2019-04-07 19:42:27	TopUp	1000000
21	2019-04-07 14:09:00	TopUp	1000000
22	2019-04-07 15:47:16	TopUp	1000000
23	2019-04-06 13:43:13	TopUp	1000000
24	2019-04-06 00:19:53	TopUp	1000000
25	2019-04-07 14:40:22	TopUp	1000000
26	2019-04-06 03:59:03	TopUp	1000000
27	2019-04-07 18:40:44	TopUp	1000000
28	2019-04-06 18:53:20	TopUp	1000000
29	2019-04-06 16:51:51	TopUp	1000000
30	2019-04-06 22:39:12	TopUp	1000000
31	2019-04-07 10:26:11	TopUp	1000000
32	2019-04-06 15:32:29	TopUp	1000000
33	2019-04-07 10:48:49	TopUp	1000000
34	2019-04-06 04:47:29	TopUp	1000000
35	2019-04-06 21:05:35	TopUp	1000000
36	2019-04-07 13:16:47	TopUp	1000000
37	2019-04-07 10:43:04	TopUp	1000000
38	2019-04-06 12:22:22	TopUp	1000000
39	2019-04-07 11:34:46	TopUp	1000000
40	2019-04-07 07:49:48	TopUp	1000000
41	2019-04-07 10:01:46	TopUp	1000000
42	2019-04-07 17:01:34	TopUp	1000000
43	2019-04-07 20:02:33	TopUp	1000000
44	2019-04-07 17:18:36	TopUp	1000000
45	2019-04-07 18:26:59	TopUp	1000000
46	2019-04-06 16:54:40	TopUp	1000000
47	2019-04-07 04:08:54	TopUp	1000000
48	2019-04-07 16:34:04	TopUp	1000000
49	2019-04-06 21:50:20	TopUp	1000000
50	2019-04-06 13:34:44	TopUp	1000000
51	2019-04-07 04:22:49	TopUp	1000000
52	2019-04-07 13:03:07	TopUp	1000000
53	2019-04-07 05:32:16	TopUp	1000000
54	2019-04-07 19:11:01	TopUp	1000000
55	2019-04-07 20:44:47	TopUp	1000000
56	2019-04-06 09:55:54	TopUp	1000000
57	2019-04-06 13:04:55	TopUp	1000000
58	2019-04-07 11:52:53	TopUp	1000000
59	2019-04-07 16:30:20	TopUp	1000000
60	2019-04-07 14:33:18	TopUp	1000000
61	2019-04-07 03:17:47	TopUp	1000000
62	2019-04-07 01:13:36	TopUp	1000000
63	2019-04-07 12:09:35	TopUp	1000000
64	2019-04-06 02:37:29	TopUp	1000000
65	2019-04-06 18:43:57	TopUp	1000000
66	2019-04-06 18:07:57	TopUp	1000000
67	2019-04-07 22:58:08	TopUp	1000000
68	2019-04-06 11:10:09	TopUp	1000000
69	2019-04-06 16:56:37	TopUp	1000000
70	2019-04-06 22:39:20	TopUp	1000000
5	2019-04-09 19:25:32	Pembayaran Pinjaman	1000
30	2019-04-09 22:51:22	Pembayaran Pinjaman	4000
67	2019-04-10 07:15:47	Pembayaran Pinjaman	10000
67	2019-04-10 07:16:47	Pembayaran Denda	100000
6	2019-04-09 09:56:11	Pembayaran Pinjaman	3000
26	2019-04-09 14:40:50	Pembayaran Pinjaman	7000
47	2019-04-09 20:21:15	Pembayaran Pinjaman	6000
39	2019-04-09 18:19:02	Pembayaran Pinjaman	3000
15	2019-04-10 16:17:18	Pembayaran Pinjaman	10000
15	2019-04-10 16:18:18	Pembayaran Denda	500000
55	2019-04-10 08:32:49	Pembayaran Pinjaman	10000
55	2019-04-10 08:33:49	Pembayaran Denda	100000
13	2019-04-09 21:05:26	Pembayaran Pinjaman	1000
45	2019-04-09 18:26:59	TopUp	1000000
45	2019-04-10 18:11:36	Pembayaran Pinjaman	10000
45	2019-04-10 18:12:36	Pembayaran Denda	1200000
18	2019-04-10 11:52:50	TopUp	1000000
18	2019-04-10 22:34:20	Pembayaran Pinjaman	10000
18	2019-04-10 22:35:20	Pembayaran Denda	1200000
58	2019-04-09 22:15:40	Pembayaran Pinjaman	5000
24	2019-04-09 18:55:08	Pembayaran Pinjaman	6000
13	2019-04-12 13:05:00	TopUp	1000000
13	2019-04-12 21:30:49	Pembayaran Pinjaman	10000
13	2019-04-12 21:31:49	Pembayaran Denda	1200000
42	2019-04-11 20:17:17	Pembayaran Pinjaman	2000
52	2019-04-10 16:41:47	Pembayaran Pinjaman	4000
7	2019-04-11 18:27:12	Pembayaran Pinjaman	9000
70	2019-04-11 16:20:06	Pembayaran Pinjaman	7000
38	2019-04-11 16:29:06	Pembayaran Pinjaman	6000
33	2019-04-11 18:54:33	Pembayaran Pinjaman	5000
1	2019-04-07 04:30:10	TopUp	10000
1	2019-04-07 04:30:11	TopUp	100000
1	2019-04-07 04:30:12	Pembayaran Pinjaman	100000
1	2019-04-07 04:30:13	Pembayaran Pinjaman	100000
\.


--
-- Data for Name: transaksi_khusus_peminjaman; Type: TABLE DATA; Schema: bike_sharing; Owner: -
--

COPY bike_sharing.transaksi_khusus_peminjaman (no_kartu_anggota, date_time, no_kartu_peminjam, datetime_pinjam, no_sepeda, id_stasiun) FROM stdin;
5	2019-04-09 19:25:32	5	2019-04-09 18:24:29	27	4
30	2019-04-09 22:51:22	30	2019-04-09 18:59:57	36	15
67	2019-04-10 07:15:47	67	2019-04-09 20:01:55	25	4
67	2019-04-10 07:16:47	67	2019-04-09 20:01:55	25	4
6	2019-04-09 09:56:11	6	2019-04-09 07:09:44	9	10
26	2019-04-09 14:40:50	26	2019-04-09 08:16:22	4	15
47	2019-04-09 20:21:15	47	2019-04-09 15:16:03	35	6
39	2019-04-09 18:19:02	39	2019-04-09 15:39:34	24	6
15	2019-04-10 16:17:18	15	2019-04-09 20:27:36	19	4
15	2019-04-10 16:18:18	15	2019-04-09 20:27:36	19	4
55	2019-04-10 08:32:49	55	2019-04-09 21:14:15	44	4
55	2019-04-10 08:33:49	55	2019-04-09 21:14:15	44	4
13	2019-04-09 21:05:26	13	2019-04-09 20:37:33	50	10
45	2019-04-10 18:11:36	45	2019-04-09 07:13:57	13	10
45	2019-04-10 18:12:36	45	2019-04-09 07:13:57	13	10
18	2019-04-10 22:34:20	18	2019-04-09 08:38:08	1	15
18	2019-04-10 22:35:20	18	2019-04-09 08:38:08	1	15
58	2019-04-09 22:15:40	58	2019-04-09 14:20:40	43	7
24	2019-04-09 18:55:08	24	2019-04-09 13:25:22	22	15
13	2019-04-12 21:30:49	13	2019-04-11 06:18:07	19	3
13	2019-04-12 21:31:49	13	2019-04-11 06:18:07	19	3
42	2019-04-11 20:17:17	42	2019-04-11 19:08:32	36	14
52	2019-04-10 16:41:47	52	2019-04-10 13:22:23	48	12
7	2019-04-11 18:27:12	7	2019-04-11 10:16:24	29	9
70	2019-04-11 16:20:06	70	2019-04-11 09:57:41	27	16
38	2019-04-11 16:29:06	38	2019-04-11 11:13:18	2	7
33	2019-04-11 18:54:33	33	2019-04-11 14:15:50	17	2
\.


--
-- Data for Name: voucher; Type: TABLE DATA; Schema: bike_sharing; Owner: -
--

COPY bike_sharing.voucher (id_voucher, nama, kategori, nilai_point, deskripsi, no_kartu_anggota) FROM stdin;
1	GammaMart	Belanja	5	Pinjaman pertama	5
2	GammaMart	Belanja	5	Pinjaman pertama	30
3	MoonBukcs Cafe	Makan	5	Pinjaman pertama	67
4	GammaMart	Belanja	5	Pinjaman pertama	6
5	GammaMart	Belanja	5	Pinjaman pertama	26
6	Wahana Dusad	Atraksi	5	Pinjaman pertama	47
7	MoonBukcs Cafe	Makan	5	Pinjaman pertama	39
8	Wahana Dusad	Atraksi	5	Pinjaman pertama	15
9	GammaMart	Belanja	5	Pinjaman pertama	55
10	MoonBukcs Cafe	Makan	5	Pinjaman pertama	13
11	GammaMart	Belanja	5	Pinjaman pertama	45
12	MoonBukcs Cafe	Makan	5	Pinjaman pertama	18
13	Wahana Dusad	Atraksi	5	Pinjaman pertama	58
14	GammaMart	Belanja	5	Pinjaman pertama	24
15	MoonBukcs Cafe	Makan	5	Pinjaman kedua	13
16	MoonBukcs Cafe	Makan	5	Pinjaman pertama	42
17	MoonBukcs Cafe	Makan	5	Pinjaman pertama	52
18	MoonBukcs Cafe	Makan	5	Pinjaman pertama	7
19	MoonBukcs Cafe	Makan	5	Pinjaman pertama	70
20	Wahana Dusad	Atraksi	5	Pinjaman pertama	38
21	Wahana Dusad	Atraksi	5	Pinjaman pertama	33
22	Wahana Dusad	Atraksi	5	Pinjaman pertama	22
23	Wahana Dusad	Atraksi	5	Pinjaman pertama	14
24	Wahana Dusad	Atraksi	5	Pinjaman kedua	67
25	GammaMart	Belanja	5	Pinjaman kedua	26
26	GammaMart	Belanja	5	Pinjaman kedua	39
27	GammaMart	Belanja	5	Pinjaman pertama	11
28	GammaMart	Belanja	5	Pinjaman pertama	57
29	GammaMart	Belanja	5	Pinjaman kedua	24
30	GammaMart	Belanja	5	Pinjaman pertama	53
31	Bakso Granat Jonggol	Makan	5	Pinjaman kedua	57
32	Bakso Granat Jonggol	Makan	5	Pinjaman pertama	62
33	Bakso Granat Jonggol	Makan	5	Pinjaman pertama	9
34	Bakso Granat Jonggol	Makan	5	Pinjaman pertama	49
35	Bakso Granat Jonggol	Makan	5	Pinjaman pertama	35
36	Bakso Granat Jonggol	Makan	5	Pinjaman pertama	36
37	Wahana Dusad	Atraksi	5	Pinjaman pertama	8
38	Wahana Dusad	Atraksi	5	Pinjaman pertama	56
39	Wahana Dusad	Atraksi	5	Pinjaman pertama	32
40	Wahana Dusad	Atraksi	5	Pinjaman pertama	2
41	Wahana Dusad	Atraksi	5	Pinjaman pertama	31
42	Wahana Dusad	Atraksi	5	Pinjaman pertama	10
43	MoonBukcs Cafe	Makan	5	Pinjaman pertama	28
44	MoonBukcs Cafe	Makan	5	Pinjaman pertama	66
45	MoonBukcs Cafe	Makan	5	Pinjaman pertama	48
46	MoonBukcs Cafe	Makan	5	Pinjaman pertama	29
47	GammaMart	Belanja	5	Pinjaman pertama	12
48	GammaMart	Belanja	5	Pinjaman kedua	30
49	GammaMart	Belanja	5	Pinjaman pertama	19
50	GammaMart	Belanja	5	Pinjaman kedua	6
100	bebas	bebas	1000	bebas	5
101	bebas	bebas	1000	bebas	5
102	bebas	bebas	1000	bebas	5
\.


--
-- Name: acara acara_pkey; Type: CONSTRAINT; Schema: bike_sharing; Owner: -
--

ALTER TABLE ONLY bike_sharing.acara
    ADD CONSTRAINT acara_pkey PRIMARY KEY (id_acara);


--
-- Name: acara_stasiun acara_stasiun_pkey; Type: CONSTRAINT; Schema: bike_sharing; Owner: -
--

ALTER TABLE ONLY bike_sharing.acara_stasiun
    ADD CONSTRAINT acara_stasiun_pkey PRIMARY KEY (id_stasiun, id_acara);


--
-- Name: anggota anggota_pkey; Type: CONSTRAINT; Schema: bike_sharing; Owner: -
--

ALTER TABLE ONLY bike_sharing.anggota
    ADD CONSTRAINT anggota_pkey PRIMARY KEY (no_kartu);


--
-- Name: laporan laporan_pkey; Type: CONSTRAINT; Schema: bike_sharing; Owner: -
--

ALTER TABLE ONLY bike_sharing.laporan
    ADD CONSTRAINT laporan_pkey PRIMARY KEY (id_laporan, no_kartu_anggota, datetime_pinjam, nomor_sepeda, id_stasiun);


--
-- Name: peminjaman peminjaman_pkey; Type: CONSTRAINT; Schema: bike_sharing; Owner: -
--

ALTER TABLE ONLY bike_sharing.peminjaman
    ADD CONSTRAINT peminjaman_pkey PRIMARY KEY (no_kartu_anggota, datetime_pinjam, nomor_sepeda, id_stasiun);


--
-- Name: penugasan penugasan_pkey; Type: CONSTRAINT; Schema: bike_sharing; Owner: -
--

ALTER TABLE ONLY bike_sharing.penugasan
    ADD CONSTRAINT penugasan_pkey PRIMARY KEY (ktp, start_datetime, id_stasiun);


--
-- Name: person person_email_key; Type: CONSTRAINT; Schema: bike_sharing; Owner: -
--

ALTER TABLE ONLY bike_sharing.person
    ADD CONSTRAINT person_email_key UNIQUE (email);


--
-- Name: person person_pkey; Type: CONSTRAINT; Schema: bike_sharing; Owner: -
--

ALTER TABLE ONLY bike_sharing.person
    ADD CONSTRAINT person_pkey PRIMARY KEY (ktp);


--
-- Name: petugas petugas_pkey; Type: CONSTRAINT; Schema: bike_sharing; Owner: -
--

ALTER TABLE ONLY bike_sharing.petugas
    ADD CONSTRAINT petugas_pkey PRIMARY KEY (ktp);


--
-- Name: sepeda sepeda_pkey; Type: CONSTRAINT; Schema: bike_sharing; Owner: -
--

ALTER TABLE ONLY bike_sharing.sepeda
    ADD CONSTRAINT sepeda_pkey PRIMARY KEY (nomor);


--
-- Name: stasiun stasiun_pkey; Type: CONSTRAINT; Schema: bike_sharing; Owner: -
--

ALTER TABLE ONLY bike_sharing.stasiun
    ADD CONSTRAINT stasiun_pkey PRIMARY KEY (id_stasiun);


--
-- Name: transaksi_khusus_peminjaman transaksi_khusus_peminjaman_pkey; Type: CONSTRAINT; Schema: bike_sharing; Owner: -
--

ALTER TABLE ONLY bike_sharing.transaksi_khusus_peminjaman
    ADD CONSTRAINT transaksi_khusus_peminjaman_pkey PRIMARY KEY (no_kartu_anggota, date_time);


--
-- Name: transaksi transaksi_pkey; Type: CONSTRAINT; Schema: bike_sharing; Owner: -
--

ALTER TABLE ONLY bike_sharing.transaksi
    ADD CONSTRAINT transaksi_pkey PRIMARY KEY (no_kartu_anggota, date_time);


--
-- Name: voucher voucher_pkey; Type: CONSTRAINT; Schema: bike_sharing; Owner: -
--

ALTER TABLE ONLY bike_sharing.voucher
    ADD CONSTRAINT voucher_pkey PRIMARY KEY (id_voucher);


--
-- Name: peminjaman add_point_anggota; Type: TRIGGER; Schema: bike_sharing; Owner: -
--

CREATE TRIGGER add_point_anggota AFTER INSERT ON bike_sharing.peminjaman FOR EACH ROW EXECUTE PROCEDURE bike_sharing.add_point_anggota();


--
-- Name: peminjaman buat_laporan_lebih_24_jam; Type: TRIGGER; Schema: bike_sharing; Owner: -
--

CREATE TRIGGER buat_laporan_lebih_24_jam BEFORE UPDATE ON bike_sharing.peminjaman FOR EACH ROW EXECUTE PROCEDURE bike_sharing.laporan_lebih_24_jam();


--
-- Name: voucher subtract_point_anggota; Type: TRIGGER; Schema: bike_sharing; Owner: -
--

CREATE TRIGGER subtract_point_anggota AFTER INSERT ON bike_sharing.voucher FOR EACH ROW EXECUTE PROCEDURE bike_sharing.subtract_point_anggota();


--
-- Name: peminjaman trigger_insert_transaksi_peminjaman; Type: TRIGGER; Schema: bike_sharing; Owner: -
--

CREATE TRIGGER trigger_insert_transaksi_peminjaman AFTER UPDATE ON bike_sharing.peminjaman FOR EACH ROW EXECUTE PROCEDURE bike_sharing.insert_transaksi_peminjaman();


--
-- Name: peminjaman update_denda; Type: TRIGGER; Schema: bike_sharing; Owner: -
--

CREATE TRIGGER update_denda BEFORE UPDATE ON bike_sharing.peminjaman FOR EACH ROW EXECUTE PROCEDURE bike_sharing.hitung_kolom_denda();


--
-- Name: peminjaman update_pengembalian; Type: TRIGGER; Schema: bike_sharing; Owner: -
--

CREATE TRIGGER update_pengembalian BEFORE UPDATE ON bike_sharing.peminjaman FOR EACH ROW EXECUTE PROCEDURE bike_sharing.insert_transaksi_khusus_peminjaman();


--
-- Name: transaksi update_saldo_anggota; Type: TRIGGER; Schema: bike_sharing; Owner: -
--

CREATE TRIGGER update_saldo_anggota AFTER INSERT ON bike_sharing.transaksi FOR EACH ROW EXECUTE PROCEDURE bike_sharing.update_saldo_anggota();


--
-- Name: acara_stasiun acara_stasiun_id_acara_fkey; Type: FK CONSTRAINT; Schema: bike_sharing; Owner: -
--

ALTER TABLE ONLY bike_sharing.acara_stasiun
    ADD CONSTRAINT acara_stasiun_id_acara_fkey FOREIGN KEY (id_acara) REFERENCES bike_sharing.acara(id_acara);


--
-- Name: acara_stasiun acara_stasiun_id_stasiun_fkey; Type: FK CONSTRAINT; Schema: bike_sharing; Owner: -
--

ALTER TABLE ONLY bike_sharing.acara_stasiun
    ADD CONSTRAINT acara_stasiun_id_stasiun_fkey FOREIGN KEY (id_stasiun) REFERENCES bike_sharing.stasiun(id_stasiun);


--
-- Name: anggota anggota_ktp_fkey; Type: FK CONSTRAINT; Schema: bike_sharing; Owner: -
--

ALTER TABLE ONLY bike_sharing.anggota
    ADD CONSTRAINT anggota_ktp_fkey FOREIGN KEY (ktp) REFERENCES bike_sharing.person(ktp);


--
-- Name: laporan laporan_no_kartu_anggota_fkey; Type: FK CONSTRAINT; Schema: bike_sharing; Owner: -
--

ALTER TABLE ONLY bike_sharing.laporan
    ADD CONSTRAINT laporan_no_kartu_anggota_fkey FOREIGN KEY (no_kartu_anggota, datetime_pinjam, nomor_sepeda, id_stasiun) REFERENCES bike_sharing.peminjaman(no_kartu_anggota, datetime_pinjam, nomor_sepeda, id_stasiun);


--
-- Name: peminjaman peminjaman_id_stasiun_fkey; Type: FK CONSTRAINT; Schema: bike_sharing; Owner: -
--

ALTER TABLE ONLY bike_sharing.peminjaman
    ADD CONSTRAINT peminjaman_id_stasiun_fkey FOREIGN KEY (id_stasiun) REFERENCES bike_sharing.stasiun(id_stasiun);


--
-- Name: peminjaman peminjaman_no_kartu_anggota_fkey; Type: FK CONSTRAINT; Schema: bike_sharing; Owner: -
--

ALTER TABLE ONLY bike_sharing.peminjaman
    ADD CONSTRAINT peminjaman_no_kartu_anggota_fkey FOREIGN KEY (no_kartu_anggota) REFERENCES bike_sharing.anggota(no_kartu);


--
-- Name: peminjaman peminjaman_nomor_sepeda_fkey; Type: FK CONSTRAINT; Schema: bike_sharing; Owner: -
--

ALTER TABLE ONLY bike_sharing.peminjaman
    ADD CONSTRAINT peminjaman_nomor_sepeda_fkey FOREIGN KEY (nomor_sepeda) REFERENCES bike_sharing.sepeda(nomor);


--
-- Name: penugasan penugasan_id_stasiun_fkey; Type: FK CONSTRAINT; Schema: bike_sharing; Owner: -
--

ALTER TABLE ONLY bike_sharing.penugasan
    ADD CONSTRAINT penugasan_id_stasiun_fkey FOREIGN KEY (id_stasiun) REFERENCES bike_sharing.stasiun(id_stasiun);


--
-- Name: penugasan penugasan_ktp_fkey; Type: FK CONSTRAINT; Schema: bike_sharing; Owner: -
--

ALTER TABLE ONLY bike_sharing.penugasan
    ADD CONSTRAINT penugasan_ktp_fkey FOREIGN KEY (ktp) REFERENCES bike_sharing.petugas(ktp);


--
-- Name: petugas petugas_ktp_fkey; Type: FK CONSTRAINT; Schema: bike_sharing; Owner: -
--

ALTER TABLE ONLY bike_sharing.petugas
    ADD CONSTRAINT petugas_ktp_fkey FOREIGN KEY (ktp) REFERENCES bike_sharing.person(ktp);


--
-- Name: sepeda sepeda_id_stasiun_fkey; Type: FK CONSTRAINT; Schema: bike_sharing; Owner: -
--

ALTER TABLE ONLY bike_sharing.sepeda
    ADD CONSTRAINT sepeda_id_stasiun_fkey FOREIGN KEY (id_stasiun) REFERENCES bike_sharing.stasiun(id_stasiun);


--
-- Name: sepeda sepeda_no_kartu_penyumbang_fkey; Type: FK CONSTRAINT; Schema: bike_sharing; Owner: -
--

ALTER TABLE ONLY bike_sharing.sepeda
    ADD CONSTRAINT sepeda_no_kartu_penyumbang_fkey FOREIGN KEY (no_kartu_penyumbang) REFERENCES bike_sharing.anggota(no_kartu);


--
-- Name: transaksi_khusus_peminjaman transaksi_khusus_peminjaman_no_kartu_anggota_fkey; Type: FK CONSTRAINT; Schema: bike_sharing; Owner: -
--

ALTER TABLE ONLY bike_sharing.transaksi_khusus_peminjaman
    ADD CONSTRAINT transaksi_khusus_peminjaman_no_kartu_anggota_fkey FOREIGN KEY (no_kartu_anggota, date_time) REFERENCES bike_sharing.transaksi(no_kartu_anggota, date_time);


--
-- Name: transaksi_khusus_peminjaman transaksi_khusus_peminjaman_no_kartu_peminjam_fkey; Type: FK CONSTRAINT; Schema: bike_sharing; Owner: -
--

ALTER TABLE ONLY bike_sharing.transaksi_khusus_peminjaman
    ADD CONSTRAINT transaksi_khusus_peminjaman_no_kartu_peminjam_fkey FOREIGN KEY (no_kartu_peminjam, datetime_pinjam, no_sepeda, id_stasiun) REFERENCES bike_sharing.peminjaman(no_kartu_anggota, datetime_pinjam, nomor_sepeda, id_stasiun);


--
-- Name: transaksi transaksi_no_kartu_anggota_fkey; Type: FK CONSTRAINT; Schema: bike_sharing; Owner: -
--

ALTER TABLE ONLY bike_sharing.transaksi
    ADD CONSTRAINT transaksi_no_kartu_anggota_fkey FOREIGN KEY (no_kartu_anggota) REFERENCES bike_sharing.anggota(no_kartu);


--
-- Name: voucher voucher_no_kartu_anggota_fkey; Type: FK CONSTRAINT; Schema: bike_sharing; Owner: -
--

ALTER TABLE ONLY bike_sharing.voucher
    ADD CONSTRAINT voucher_no_kartu_anggota_fkey FOREIGN KEY (no_kartu_anggota) REFERENCES bike_sharing.anggota(no_kartu);


--
-- Name: SCHEMA public; Type: ACL; Schema: -; Owner: -
--

REVOKE ALL ON SCHEMA public FROM PUBLIC;
REVOKE ALL ON SCHEMA public FROM postgres;
GRANT ALL ON SCHEMA public TO postgres;
GRANT ALL ON SCHEMA public TO PUBLIC;


--
-- PostgreSQL database dump complete
--

