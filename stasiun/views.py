from django.shortcuts import render, redirect
from .models import Stasiun

def get_as_dict(cursor):
    columns = [column[0] for column in cursor.description]
    results = []
    for row in cursor.fetchall():
        results.append(dict(zip(columns, row)))
    return results

def index(request):
    context = {
        "stations" : get_as_dict(Stasiun.get_all_stasiun())
    }
    return render(request, 'stasiun-index.html', context)

def edit(request, id):
    context = {
        "stasiun" : get_as_dict(Stasiun.get_stasiun_by_id_stasiun(id))
    }
    print(context)
    return render(request, 'stasiun-edit.html', context)

def create(request):
    return render(request, 'stasiun-create.html')


def store(request):
    stasiun = {
            "alamat" : request.POST.get('alamat'),
            "lat" : request.POST.get('lat'),
            "long" : request.POST.get('long'),
            "nama" : request.POST.get('nama'),
    }
    Stasiun.insert_stasiun(stasiun)
    return redirect('/stasiun/')

def update(request, id):
    stasiun = {
            "alamat" : request.POST.get('alamat'),
            "lat" : request.POST.get('lat'),
            "long" : request.POST.get('long'),
            "nama" : request.POST.get('nama'),
    }
    Stasiun.update_stasiun(stasiun, id)
    return redirect('/stasiun/')


def delete(request, id):
    Stasiun.delete_stasiun(stasiun, id)
    return redirect('/stasiun/')
