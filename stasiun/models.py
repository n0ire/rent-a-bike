from django.db import models

# Create your models here.
from django.db import models
from django.db import connection

# Create your models here.
SCHEMA = "bike_sharing"

SEPEDA = SCHEMA + "." + "sepeda"
STASIUN = SCHEMA + "." + "stasiun"
ANGGOTA = SCHEMA + "." + "anggota"
PERSON = SCHEMA + "." + "person"

cursor =  connection.cursor()

class Stasiun:

    def get_all_stasiun():
        query = 'select * from %s order by id_stasiun;' % (STASIUN)
        cursor.execute(query)
        return cursor

    def get_stasiun_by_id_stasiun(id):
        query = '''select * from %s where id_stasiun = '%s' ;''' % (STASIUN, id)
        cursor.execute(query)
        return cursor

    def insert_stasiun(stasiun):
        query =  '''
            INSERT INTO %s (alamat, lat, long, nama)
            VALUES ('%s', '%s', '%s', '%s', '%s');
        ''' %(STASIUN, stasiun['alamat'], stasiun['lat'], stasiun['long'], stasiun['nama'])
        cursor.execute(query)
        return cursor

    def update_stasiun(stasiun, id_stasiun):
        query =  '''
            UPDATE %s
            set alamat = '%s', lat = '%s', long = '%s', nama = '%s'
            where id_stasiun = '%s';
        ''' %(STASIUN, stasiun['alamat'], stasiun['lat'], stasiun['long'], stasiun['nama'], id_stasiun)
        cursor.execute(query)
        return cursor

    def delete_stasiun(id_stasiun):
        query =  '''
            DELETE FROM %s WHERE nomor = '%s';
        ''' %(STASIUN, id_stasiun)
        cursor.execute(query)
        return cursor


    # def get_sepeda_and_join_stasiun_penyumbang():
    #     query = '''select CAST (sp.nomor AS INTEGER) as nomor, sp.merk as merk,
    #                 sp.status as status, sp.jenis as jenis,
    #                 st.nama as nama_st, p.nama as nama_penyumbang
    #                 from %s as sp,
    #                 %s as st,
    #                 %s as a,
    #                 %s as p
    #                 where sp.id_stasiun = st.id_stasiun
    #                 and sp.no_kartu_penyumbang = a.no_kartu
    #                 and a.ktp = p.ktp
    #                 order by nomor asc
    #                 ;
    #             ''' % (SEPEDA, STASIUN, ANGGOTA, PERSON)
    #     cursor.execute(query)
    #     return cursor
