"""rent_a_bike URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
import voucher.urls as voucher
import peminjaman.urls as peminjaman
from django.urls import path, include
from django.urls import path, include
from django.urls import path, include, reverse
from django.conf import settings
from django.conf.urls.static import static

from . import views

urlpatterns = [
    path('stasiun/', include(('stasiun.urls', 'stasiun'), namespace='stasiun')),
    path('sepeda/', include(('sepeda.urls', 'sepeda'), namespace='sepeda')),
    path('penugasan/', include(('penugasan.urls', 'penugasan'), namespace='penugasan')),
    path('acara/', include(('acara.urls', 'acara'), namespace='acara')),
    path('voucher/', include(voucher)),
    path('peminjaman/', include(peminjaman)),
    path('transaksi/', include('transaksi.urls')),
    path('auth/', include('gauth.urls')),
    path('', views.index)
] 
urlpatterns += static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)
