from django.urls import path
from django.conf.urls import url, include
from .views import *
from django.contrib.staticfiles.urls import staticfiles_urlpatterns

#url for app

urlpatterns = [
    path('', index, name = "index"),
    path('form-peminjaman', create, name = "form_peminjaman")
]
