from django.db import models
from django.db import connection

# Create your models here.
SCHEMA = "bike_sharing"

ANGGOTA = SCHEMA + "." + "anggota"
PEMINJAMAN = SCHEMA + "." + "peminjaman"
SEPEDA = SCHEMA + "." + "sepeda"
STASIUN = SCHEMA + "." + "stasiun"

cursor =  connection.cursor()

class Peminjaman:

    def get_all_peminjaman():
        query = 'select * from %s ;' % (peminjaman)
        cursor.execute(query)
        return cursor

    def get_peminjaman_by_id(member, sepeda):
        query = 'select * from %s where no_kartu_anggota = \'%d\' and nomor_sepeda = \'%d\';' % (PEMINJAMAN, member, sepeda)
        cursor.execute(query)
        return cursor

    def get_peminjaman_and_join_anggota_sepeda():
        query = '''select CAST (pm.no_kartu_anggota AS INTEGER) as no_kartu_anggota, pm.nomor_sepeda as nomor_sepeda, sp.merk as merk_sepeda,
                    pm.id_stasiun as id_stasiun, st.nama as nama_stasiun,
                    pm.datetime_kembali as datetime_kembali, pm.biaya as biaya, pm.denda as denda
                    from %s as pm,
                    %s as a,
                    %s as sp,
                    %s as st
                    where pm.no_kartu_anggota = a.no_kartu
                    and pm.nomor_sepeda = sp.nomor
                    and pm.id_stasiun = st.id_stasiun
                    order by no_kartu_anggota asc
                    ;
                ''' % (PEMINJAMAN, ANGGOTA, SEPEDA, STASIUN)
        cursor.execute(query)
        return cursor

    def get_all_stasiun():
    	query = 'select * from %s ;' % (STASIUN)
    	cursor.execute(query)
    	return cursor

# Create your models here.
