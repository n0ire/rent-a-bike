# Create your views here.

from django.shortcuts import render
from .models import Peminjaman

def get_as_dict(cursor):
    columns = [column[0] for column in cursor.description]
    results = []
    for row in cursor.fetchall():
        results.append(dict(zip(columns, row)))
    return results

def index(request):
    context = {
        "peminjaman" : get_as_dict(Peminjaman.get_peminjaman_and_join_anggota_sepeda())
    }
    # print(len(context["sepedas"]))
    # print(context["sepedas"])
    return render(request, 'daftar_peminjaman.html', context)
def create(request):
	context = {
        "stasiuns" : get_as_dict(Peminjaman.get_all_stasiun())
    } 
	return render(request, 'form_peminjaman.html', context)